using System;
using System.Collections.Specialized;
using System.IO;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;

namespace TAVIA.SharePoint.Club0803.Features.Club0803
{
    /// <summary>
    /// This class handles events raised during feature activation, deactivation, installation, uninstallation, and upgrade.
    /// </summary>
    /// <remarks>
    /// The GUID attached to this class may be used during packaging and should not be modified.
    /// </remarks>

    [Guid("9353a71b-07a4-422e-a5e8-f741c3d45c0e")]
    public class Club0803EventReceiver : SPFeatureReceiver
    {
        public override void FeatureActivated(SPFeatureReceiverProperties properties)
        {
            SPWeb web = (SPWeb)properties.Feature.Parent;
            AddActorsGroups(web);

            SPList CategoriesList = AddCategoriesList(web);
            SPList CategoryTypesList = AddCategoryTypesList(web, CategoriesList);
            SPList VendorsList = AddVendorsList(web);
            SPList VendorSuggestionsList = AddVendorSuggestionsList(web);
            SPList CardsList = AddCardsList(web, VendorsList);
            SPList CardRequestsList = AddCardRequestsList(web, VendorsList);

            SPList DiscountedItemsList = AddDiscountedItemsList(web, properties, VendorsList, CategoriesList, CategoryTypesList);
            SPList OrdersList = AddOrdersList(web, DiscountedItemsList);
            SPList OrdersCartList = AddOrdersCartList(web, DiscountedItemsList);
        }

        private SPList AddCategoriesList(SPWeb web)
        {
            string CategoriesListName = SPUtility.GetLocalizedString("$Resources:CategoriesListName", "Club0803_Config", 1033);
            string CategoriesListInternalName = SPUtility.GetLocalizedString("$Resources:CategoriesListUrl", "Club0803_Config", 1033);
            SPList CategoriesList = null;
            try
            {
                string listUrl = String.Format("{0}/lists/{1}", web.Url, CategoriesListInternalName);
                CategoriesList = web.GetList(listUrl);
            }
            catch (FileNotFoundException e)
            {
                CategoriesList = null;
            }

            if (CategoriesList == null)
            {
                Guid listID = web.Lists.Add(CategoriesListInternalName, String.Empty, SPListTemplateType.GenericList);
                CategoriesList = web.Lists[listID];
                CategoriesList.Title = CategoriesListName;
                CategoriesList.OnQuickLaunch = false;
                //VendorsList.DisableGridEditing = true;

                CategoriesList.Update();

                SPFieldText CategoryField = (SPFieldText)CategoriesList.Fields.GetFieldByInternalName("Title");
                //CategoryField.StaticName = "Category";
                CategoryField.Title = "Category";
                CategoryField.Update();

                ////make new columns visible in default view
                //SPView view = CategoriesList.DefaultView;
                //view.Query = "<OrderBy><FieldRef Name=\"ID\" Ascending=\"FALSE\" /></OrderBy>";
                //view.Update();

                SPListItem category1 = CategoriesList.AddItem();
                category1["Title"] = "Products/Services";
                category1.Update();

                SPListItem category2 = CategoriesList.AddItem();
                category2["Title"] = "Benefits";
                category2.Update();

                //Granting permissions to security groups
                string AdminsGroupName = SPUtility.GetLocalizedString("$Resources:AdminsGroupName", "Club0803_Config", 1033);
                string SuperAdminsGroupName = SPUtility.GetLocalizedString("$Resources:SuperAdminsGroupName", "Club0803_Config", 1033);
                string PermanentStaffGroupName = SPUtility.GetLocalizedString("$Resources:PermanentStaffGroupName", "Club0803_Config", 1033);

                if (!CategoriesList.HasUniqueRoleAssignments)
                {
                    CategoriesList.BreakRoleInheritance(false);
                }

                SPGroup AdminsGroup = web.SiteGroups[AdminsGroupName];
                if (AdminsGroup != null)
                {
                    SPPrincipal AdminsGroupPrincipal = AdminsGroup as SPPrincipal;
                    SPRoleAssignment newRoleAssignmentToAdd = new SPRoleAssignment(AdminsGroupPrincipal);
                    SPRoleDefinition roleDefinition = web.RoleDefinitions.GetByType(SPRoleType.Contributor);
                    newRoleAssignmentToAdd.RoleDefinitionBindings.Add(roleDefinition);
                    CategoriesList.RoleAssignments.Add(newRoleAssignmentToAdd);
                }

                SPGroup SuperAdminsGroup = web.SiteGroups[SuperAdminsGroupName];
                if (SuperAdminsGroup != null)
                {
                    SPPrincipal SuperAdminsGroupPrincipal = SuperAdminsGroup as SPPrincipal;
                    SPRoleAssignment newRoleAssignmentToAdd = new SPRoleAssignment(SuperAdminsGroupPrincipal);
                    SPRoleDefinition roleDefinition = web.RoleDefinitions.GetByType(SPRoleType.Contributor);
                    newRoleAssignmentToAdd.RoleDefinitionBindings.Add(roleDefinition);
                    CategoriesList.RoleAssignments.Add(newRoleAssignmentToAdd);
                }

                SPGroup PermanentStaffGroup = web.SiteGroups[PermanentStaffGroupName];
                if (PermanentStaffGroup != null)
                {
                    SPPrincipal PermanentStaffGroupPrincipal = PermanentStaffGroup as SPPrincipal;
                    SPRoleAssignment newRoleAssignmentToAdd = new SPRoleAssignment(PermanentStaffGroupPrincipal);
                    SPRoleDefinition roleDefinition = web.RoleDefinitions.GetByType(SPRoleType.Reader);
                    newRoleAssignmentToAdd.RoleDefinitionBindings.Add(roleDefinition);
                    CategoriesList.RoleAssignments.Add(newRoleAssignmentToAdd);
                }
            }
            return CategoriesList;
        }

        private SPList AddCategoryTypesList(SPWeb web, SPList categoriesList)
        {
            string CategoryTypesListName = SPUtility.GetLocalizedString("$Resources:CategoryTypesListName", "Club0803_Config", 1033);
            string CategoryTypesListInternalName = SPUtility.GetLocalizedString("$Resources:CategoryTypesListUrl", "Club0803_Config", 1033);
            SPList CategoryTypesList = null;
            try
            {
                string listUrl = String.Format("{0}/lists/{1}", web.Url, CategoryTypesListInternalName);
                CategoryTypesList = web.GetList(listUrl);
            }
            catch (FileNotFoundException e)
            {
                CategoryTypesList = null;
            }

            if (CategoryTypesList == null)
            {
                Guid listID = web.Lists.Add(CategoryTypesListInternalName, String.Empty, SPListTemplateType.GenericList);
                CategoryTypesList = web.Lists[listID];
                CategoryTypesList.Title = CategoryTypesListName;
                CategoryTypesList.OnQuickLaunch = false;
                //VendorsList.DisableGridEditing = true;

                CategoryTypesList.Update();

                SPFieldText CategoryField = (SPFieldText)CategoryTypesList.Fields.GetFieldByInternalName("Title");
                //CategoryField.StaticName = "CategoryType";
                CategoryField.Title = "Category Type";
                CategoryField.Update();

                CategoryTypesList.Fields.AddLookup("Category", categoriesList.ID, true);
                SPFieldLookup fieldVendor = (SPFieldLookup)CategoryTypesList.Fields.GetFieldByInternalName("Category");
                fieldVendor.Title = "Category";
                fieldVendor.LookupField = "Title";
                fieldVendor.LookupWebId = web.ID;
                fieldVendor.Indexed = true;
                fieldVendor.RelationshipDeleteBehavior = SPRelationshipDeleteBehavior.Restrict;
                fieldVendor.Update();

                //make new columns visible in default view
                SPView view = CategoryTypesList.DefaultView;
                view.ViewFields.Add("Category");
                //view.Query = "<OrderBy><FieldRef Name=\"ID\" Ascending=\"FALSE\" /></OrderBy>";
                view.Update();

                SPFieldLookupValue productsCategory = new SPFieldLookupValue(1, "Products/Services");
                SPFieldLookupValue benefitsCategory = new SPFieldLookupValue(2, "Benefits");

                SPListItem item1 = CategoryTypesList.AddItem();
                item1["Title"] = "Personal Loans";
                item1["Category"] = benefitsCategory;
                item1.Update();

                SPListItem item2 = CategoryTypesList.AddItem();
                item2["Title"] = "Mortgage & Mortgage Refinancing";
                item2["Category"] = benefitsCategory;
                item2.Update();

                SPListItem item3 = CategoryTypesList.AddItem();
                item3["Title"] = "Credit Cards";
                item3["Category"] = benefitsCategory;
                item3.Update();

                SPListItem item4 = CategoryTypesList.AddItem();
                item4["Title"] = "Vehicle Financing";
                item4["Category"] = benefitsCategory;
                item4.Update();

                SPListItem item5 = CategoryTypesList.AddItem();
                item5["Title"] = "Travels & Holidays";
                item5["Category"] = benefitsCategory;
                item5.Update();

                SPListItem item6 = CategoryTypesList.AddItem();
                item6["Title"] = "Health & Wellness";
                item6["Category"] = benefitsCategory;
                item6.Update();

                SPListItem item7 = CategoryTypesList.AddItem();
                item7["Title"] = "Fashion";
                item7["Category"] = benefitsCategory;
                item7.Update();

                SPListItem item8 = CategoryTypesList.AddItem();
                item8["Title"] = "Electronics/Gadgets";
                item8["Category"] = benefitsCategory;
                item8.Update();

                SPListItem item9 = CategoryTypesList.AddItem();
                item9["Title"] = "Home & Kitchen";
                item9["Category"] = benefitsCategory;
                item9.Update();

                SPListItem item10 = CategoryTypesList.AddItem();
                item10["Title"] = "Insurance Products";
                item10["Category"] = benefitsCategory;
                item10.Update();

                SPListItem item11 = CategoryTypesList.AddItem();
                item11["Title"] = "Other";
                item11["Category"] = benefitsCategory;
                item11.Update();

                SPListItem item12 = CategoryTypesList.AddItem();
                item12["Title"] = "Electronic";
                item12["Category"] = productsCategory;
                item12.Update();

                SPListItem item13 = CategoryTypesList.AddItem();
                item13["Title"] = "Furniture";
                item13["Category"] = productsCategory;
                item13.Update();

                SPListItem item14 = CategoryTypesList.AddItem();
                item14["Title"] = "Computing";
                item14["Category"] = productsCategory;
                item14.Update();

                SPListItem item15 = CategoryTypesList.AddItem();
                item15["Title"] = "Phone & Tablet";
                item15["Category"] = productsCategory;
                item15.Update();

                SPListItem item16 = CategoryTypesList.AddItem();
                item16["Title"] = "Baby Products";
                item16["Category"] = productsCategory;
                item16.Update();

                //Granting permissions to security groups
                string AdminsGroupName = SPUtility.GetLocalizedString("$Resources:AdminsGroupName", "Club0803_Config", 1033);
                string SuperAdminsGroupName = SPUtility.GetLocalizedString("$Resources:SuperAdminsGroupName", "Club0803_Config", 1033);
                string PermanentStaffGroupName = SPUtility.GetLocalizedString("$Resources:PermanentStaffGroupName", "Club0803_Config", 1033);

                if (!CategoryTypesList.HasUniqueRoleAssignments)
                {
                    CategoryTypesList.BreakRoleInheritance(false);
                }

                SPGroup AdminsGroup = web.SiteGroups[AdminsGroupName];
                if (AdminsGroup != null)
                {
                    SPPrincipal AdminsGroupPrincipal = AdminsGroup as SPPrincipal;
                    SPRoleAssignment newRoleAssignmentToAdd = new SPRoleAssignment(AdminsGroupPrincipal);
                    SPRoleDefinition roleDefinition = web.RoleDefinitions.GetByType(SPRoleType.Contributor);
                    newRoleAssignmentToAdd.RoleDefinitionBindings.Add(roleDefinition);
                    CategoryTypesList.RoleAssignments.Add(newRoleAssignmentToAdd);
                }

                SPGroup SuperAdminsGroup = web.SiteGroups[SuperAdminsGroupName];
                if (SuperAdminsGroup != null)
                {
                    SPPrincipal SuperAdminsGroupPrincipal = SuperAdminsGroup as SPPrincipal;
                    SPRoleAssignment newRoleAssignmentToAdd = new SPRoleAssignment(SuperAdminsGroupPrincipal);
                    SPRoleDefinition roleDefinition = web.RoleDefinitions.GetByType(SPRoleType.Contributor);
                    newRoleAssignmentToAdd.RoleDefinitionBindings.Add(roleDefinition);
                    CategoryTypesList.RoleAssignments.Add(newRoleAssignmentToAdd);
                }

                SPGroup PermanentStaffGroup = web.SiteGroups[PermanentStaffGroupName];
                if (PermanentStaffGroup != null)
                {
                    SPPrincipal PermanentStaffGroupPrincipal = PermanentStaffGroup as SPPrincipal;
                    SPRoleAssignment newRoleAssignmentToAdd = new SPRoleAssignment(PermanentStaffGroupPrincipal);
                    SPRoleDefinition roleDefinition = web.RoleDefinitions.GetByType(SPRoleType.Reader);
                    newRoleAssignmentToAdd.RoleDefinitionBindings.Add(roleDefinition);
                    CategoryTypesList.RoleAssignments.Add(newRoleAssignmentToAdd);
                }
            }
            return CategoryTypesList;
        }

        private SPList AddVendorsList(SPWeb web)
        {
            string VendorsListName = SPUtility.GetLocalizedString("$Resources:VendorsListName", "Club0803_Config", 1033);
            string VendorsListInternalName = SPUtility.GetLocalizedString("$Resources:VendorsListUrl", "Club0803_Config", 1033);
            SPList VendorsList = null;
            try
            {
                string listUrl = String.Format("{0}/lists/{1}", web.Url, VendorsListInternalName);
                VendorsList = web.GetList(listUrl);
            }
            catch (FileNotFoundException e)
            {
                VendorsList = null;
            }

            if (VendorsList == null)
            {
                Guid listID = web.Lists.Add(VendorsListInternalName, String.Empty, SPListTemplateType.GenericList);
                VendorsList = web.Lists[listID];
                VendorsList.Title = VendorsListName;
                VendorsList.OnQuickLaunch = false;
                //VendorsList.DisableGridEditing = true;
                //VendorsList.BreakRoleInheritance(false);
                //SPUser allusers = web.EnsureUser("NT AUTHORITY\\Authenticated Users");
                //SPRoleAssignment newRoleAssignmentToAdd = new SPRoleAssignment(allusers);
                //SPRoleDefinition _roleDefinition = web.RoleDefinitions.GetByType(SPRoleType.Reader);
                //newRoleAssignmentToAdd.RoleDefinitionBindings.Add(_roleDefinition);
                //VendorsList.RoleAssignments.Add(newRoleAssignmentToAdd);

                VendorsList.Update();

                SPFieldText CategoryField = (SPFieldText)VendorsList.Fields.GetFieldByInternalName("Title");
                CategoryField.StaticName = "VendorName";
                CategoryField.Title = "Vendor Name";
                CategoryField.Update();

                //Vendor Name
                //Vendor Phone Number
                //Vendor Email
                //Vendor Contact Person
                //Vendor Contact Address

                //PhoneNumber
                VendorsList.Fields.Add("PhoneNumber", SPFieldType.Text, true);
                SPFieldText fieldPhoneNumber = (SPFieldText)VendorsList.Fields.GetFieldByInternalName("PhoneNumber");
                fieldPhoneNumber.Title = "Phone Number";
                fieldPhoneNumber.MaxLength = 13;
                fieldPhoneNumber.ValidationFormula = "=OR(AND(LEN([Phone Number])=11,ISNUMBER([Phone Number]+0)),AND(LEN([Phone Number])=13,ISNUMBER([Phone Number]+0)))";
                fieldPhoneNumber.ValidationMessage = "Please enter a valid phone number";
                fieldPhoneNumber.Update();

                //Email
                VendorsList.Fields.Add("Email", SPFieldType.Text, true);
                SPFieldText fieldEmail = (SPFieldText)VendorsList.Fields.GetFieldByInternalName("Email");
                fieldEmail.MaxLength = 255;
                fieldEmail.ValidationFormula = "=(LEN(LEFT([Email],FIND(\"@\",[Email])-1))>0)+(LEN(RIGHT([Email],LEN([Email])-FIND(\".\",[Email],FIND(\"@\",[Email]))))>0)+(LEN(MID([Email],FIND(\"@\",[Email])+1,FIND(\".\",[Email],FIND(\"@\",[Email]))-FIND(\"@\",[Email])-1))>0)+(ISERROR(FIND(\" \",[Email]))=TRUE)";
                fieldEmail.ValidationMessage = "Please enter a valid email address";
                fieldEmail.Update();

                //ContactPerson
                VendorsList.Fields.Add("ContactPerson", SPFieldType.Text, true);
                SPFieldText fieldContactPerson = (SPFieldText)VendorsList.Fields.GetFieldByInternalName("ContactPerson");
                fieldContactPerson.Title = "Contact Person";
                fieldContactPerson.MaxLength = 255;
                fieldContactPerson.Update();

                //Contact Address
                VendorsList.Fields.Add("ContactAddress", SPFieldType.Note, true);
                SPFieldMultiLineText fieldContactAddress = (SPFieldMultiLineText)VendorsList.Fields.GetFieldByInternalName("ContactAddress");
                fieldContactAddress.Title = "Contact Address";
                fieldContactAddress.RichText = false;
                fieldContactAddress.NumberOfLines = 6;
                fieldContactAddress.Update();

                //make new columns visible in default view
                SPView view = VendorsList.DefaultView;
                view.ViewFields.Add("PhoneNumber");
                view.ViewFields.Add("Email");
                view.ViewFields.Add("ContactPerson");
                view.ViewFields.Add("ContactAddress");
                view.Query = "<OrderBy><FieldRef Name=\"ID\" Ascending=\"FALSE\" /></OrderBy>";
                view.Update();

                StringCollection colViewFields = new StringCollection();
                colViewFields.Add("PhoneNumber");
                colViewFields.Add("Email");
                colViewFields.Add("ContactPerson");
                colViewFields.Add("ContactAddress");
                string viewQuery = "<OrderBy><FieldRef Name=\"ID\" Ascending=\"FALSE\" /></OrderBy>";
                VendorsList.Views.Add("Report", colViewFields, viewQuery, 100, true, false);



                //Granting permissions to security groups
                string AdminsGroupName = SPUtility.GetLocalizedString("$Resources:AdminsGroupName", "Club0803_Config", 1033);
                string SuperAdminsGroupName = SPUtility.GetLocalizedString("$Resources:SuperAdminsGroupName", "Club0803_Config", 1033);
                string PermanentStaffGroupName = SPUtility.GetLocalizedString("$Resources:PermanentStaffGroupName", "Club0803_Config", 1033);

                if (!VendorsList.HasUniqueRoleAssignments)
                {
                    VendorsList.BreakRoleInheritance(false);
                }

                SPGroup AdminsGroup = web.SiteGroups[AdminsGroupName];
                if (AdminsGroup != null)
                {
                    SPPrincipal AdminsGroupPrincipal = AdminsGroup as SPPrincipal;
                    SPRoleAssignment newRoleAssignmentToAdd = new SPRoleAssignment(AdminsGroupPrincipal);
                    SPRoleDefinition roleDefinition = web.RoleDefinitions.GetByType(SPRoleType.Contributor);
                    newRoleAssignmentToAdd.RoleDefinitionBindings.Add(roleDefinition);
                    VendorsList.RoleAssignments.Add(newRoleAssignmentToAdd);
                }

                SPGroup SuperAdminsGroup = web.SiteGroups[SuperAdminsGroupName];
                if (SuperAdminsGroup != null)
                {
                    SPPrincipal SuperAdminsGroupPrincipal = SuperAdminsGroup as SPPrincipal;
                    SPRoleAssignment newRoleAssignmentToAdd = new SPRoleAssignment(SuperAdminsGroupPrincipal);
                    SPRoleDefinition roleDefinition = web.RoleDefinitions.GetByType(SPRoleType.Contributor);
                    newRoleAssignmentToAdd.RoleDefinitionBindings.Add(roleDefinition);
                    VendorsList.RoleAssignments.Add(newRoleAssignmentToAdd);
                }

                SPGroup PermanentStaffGroup = web.SiteGroups[PermanentStaffGroupName];
                if (PermanentStaffGroup != null)
                {
                    SPPrincipal PermanentStaffGroupPrincipal = PermanentStaffGroup as SPPrincipal;
                    SPRoleAssignment newRoleAssignmentToAdd = new SPRoleAssignment(PermanentStaffGroupPrincipal);
                    SPRoleDefinition roleDefinition = web.RoleDefinitions.GetByType(SPRoleType.Reader);
                    newRoleAssignmentToAdd.RoleDefinitionBindings.Add(roleDefinition);
                    VendorsList.RoleAssignments.Add(newRoleAssignmentToAdd);
                }
            }

            return VendorsList;
        }

        private SPList AddVendorSuggestionsList(SPWeb web)
        {
            string VendorSuggestionsListName = SPUtility.GetLocalizedString("$Resources:VendorSuggestionsListName", "Club0803_Config", 1033);
            string VendorSuggestionsListInternalName = SPUtility.GetLocalizedString("$Resources:VendorSuggestionsListUrl", "Club0803_Config", 1033);
            SPList VendorSuggestionsList = null;
            try
            {
                string listUrl = String.Format("{0}/lists/{1}", web.Url, VendorSuggestionsListInternalName);
                VendorSuggestionsList = web.GetList(listUrl);
            }
            catch (FileNotFoundException e)
            {
                VendorSuggestionsList = null;
            }

            if (VendorSuggestionsList == null)
            {
                Guid listID = web.Lists.Add(VendorSuggestionsListInternalName, String.Empty, SPListTemplateType.GenericList);
                VendorSuggestionsList = web.Lists[listID];
                VendorSuggestionsList.Title = VendorSuggestionsListName;
                VendorSuggestionsList.OnQuickLaunch = false;
                //VendorsList.DisableGridEditing = true;

                VendorSuggestionsList.Update();

                SPFieldText CategoryField = (SPFieldText)VendorSuggestionsList.Fields.GetFieldByInternalName("Title");
                CategoryField.StaticName = "VendorName";
                CategoryField.Title = "Vendor Name";
                CategoryField.Update();

                //List Title(Vendor Suggestions/Recommendations)
                //Vendor Name
                //Vendor Phone Number(Optional)
                //Vendor Website URL(Optional)
                //Vendor Location(Optional)
                //Comments(Multiline/optional)

                //PhoneNumber
                VendorSuggestionsList.Fields.Add("PhoneNumber", SPFieldType.Text, false);
                SPFieldText fieldPhoneNumber = (SPFieldText)VendorSuggestionsList.Fields.GetFieldByInternalName("PhoneNumber");
                fieldPhoneNumber.Title = "Phone Number";
                fieldPhoneNumber.MaxLength = 13;
                //fieldPhoneNumber.ValidationFormula = "=OR(AND(LEN([Phone Number])=11,ISNUMBER([Phone Number]+0)),AND(LEN([Phone Number])=13,ISNUMBER([Phone Number]+0)))";
                fieldPhoneNumber.ValidationFormula = "=OR(ISBLANK([Phone Number]),(OR(AND(LEN([Phone Number])=11,ISNUMBER([Phone Number]+0)),AND(LEN([Phone Number])=13,ISNUMBER([Phone Number]+0)))))";
                fieldPhoneNumber.ValidationMessage = "Please enter a valid phone number";
                fieldPhoneNumber.Update();

                //WebsiteURL
                VendorSuggestionsList.Fields.Add("WebsiteURL", SPFieldType.Text, false);
                SPFieldText fieldEmail = (SPFieldText)VendorSuggestionsList.Fields.GetFieldByInternalName("WebsiteURL");
                fieldEmail.MaxLength = 255;
                fieldEmail.Title = "Website URL";
                fieldEmail.Update();

                //VendorLocation
                VendorSuggestionsList.Fields.Add("VendorLocation", SPFieldType.Text, false);
                SPFieldText fieldContactPerson = (SPFieldText)VendorSuggestionsList.Fields.GetFieldByInternalName("VendorLocation");
                fieldContactPerson.Title = "Vendor Location";
                fieldContactPerson.MaxLength = 255;
                fieldContactPerson.Update();

                //Employee Name(Automatically pick from current user)
                VendorSuggestionsList.Fields.Add("EmployeeName", SPFieldType.User, true);
                SPFieldUser fieldEmployeeName = (SPFieldUser)VendorSuggestionsList.Fields.GetFieldByInternalName("EmployeeName");
                fieldEmployeeName.AllowMultipleValues = false;
                fieldEmployeeName.SelectionMode = SPFieldUserSelectionMode.PeopleOnly;
                fieldEmployeeName.LookupField = "ImnName";
                fieldEmployeeName.Title = "Employee Name";
                fieldEmployeeName.Update();

                //Comments
                VendorSuggestionsList.Fields.Add("Comments", SPFieldType.Note, false);
                SPFieldMultiLineText fieldContactAddress = (SPFieldMultiLineText)VendorSuggestionsList.Fields.GetFieldByInternalName("Comments");
                fieldContactAddress.Title = "Comments";
                fieldContactAddress.RichText = false;
                fieldContactAddress.NumberOfLines = 6;
                fieldContactAddress.Update();

                //make new columns visible in default view
                SPView view = VendorSuggestionsList.DefaultView;
                view.ViewFields.Add("PhoneNumber");
                view.ViewFields.Add("WebsiteURL");
                view.ViewFields.Add("VendorLocation");
                view.ViewFields.Add("EmployeeName");
                view.ViewFields.Add("Comments");
                view.Query = "<OrderBy><FieldRef Name=\"ID\" Ascending=\"FALSE\" /></OrderBy>";
                view.Update();

                //Granting permissions to security groups
                string AdminsGroupName = SPUtility.GetLocalizedString("$Resources:AdminsGroupName", "Club0803_Config", 1033);
                string SuperAdminsGroupName = SPUtility.GetLocalizedString("$Resources:SuperAdminsGroupName", "Club0803_Config", 1033);

                if (!VendorSuggestionsList.HasUniqueRoleAssignments)
                {
                    VendorSuggestionsList.BreakRoleInheritance(false);
                }

                SPGroup AdminsGroup = web.SiteGroups[AdminsGroupName];
                if (AdminsGroup != null)
                {
                    SPPrincipal AdminsGroupPrincipal = AdminsGroup as SPPrincipal;
                    SPRoleAssignment newRoleAssignmentToAdd = new SPRoleAssignment(AdminsGroupPrincipal);
                    SPRoleDefinition roleDefinition = web.RoleDefinitions.GetByType(SPRoleType.Reader);
                    newRoleAssignmentToAdd.RoleDefinitionBindings.Add(roleDefinition);
                    VendorSuggestionsList.RoleAssignments.Add(newRoleAssignmentToAdd);
                }

                SPGroup SuperAdminsGroup = web.SiteGroups[SuperAdminsGroupName];
                if (SuperAdminsGroup != null)
                {
                    SPPrincipal SuperAdminsGroupPrincipal = SuperAdminsGroup as SPPrincipal;
                    SPRoleAssignment newRoleAssignmentToAdd = new SPRoleAssignment(SuperAdminsGroupPrincipal);
                    SPRoleDefinition roleDefinition = web.RoleDefinitions.GetByType(SPRoleType.Reader);
                    newRoleAssignmentToAdd.RoleDefinitionBindings.Add(roleDefinition);
                    VendorSuggestionsList.RoleAssignments.Add(newRoleAssignmentToAdd);
                }
            }
            return VendorSuggestionsList;
        }

        private SPList AddCardsList(SPWeb web, SPList vendorsList)
        {
            string CardsListName = SPUtility.GetLocalizedString("$Resources:CardsListName", "Club0803_Config", 1033);
            string CardsListInternalName = SPUtility.GetLocalizedString("$Resources:CardsListUrl", "Club0803_Config", 1033);
            SPList CardsList = null;
            try
            {
                string listUrl = String.Format("{0}/lists/{1}", web.Url, CardsListInternalName);
                CardsList = web.GetList(listUrl);
            }
            catch (FileNotFoundException e)
            {
                CardsList = null;
            }

            if (CardsList == null)
            {
                Guid listID = web.Lists.Add(CardsListInternalName, String.Empty, SPListTemplateType.GenericList);
                CardsList = web.Lists[listID];
                CardsList.Title = CardsListName;
                CardsList.OnQuickLaunch = false;
                //VendorsList.DisableGridEditing = true;
                //VendorsList.BreakRoleInheritance(false);
                //SPUser allusers = web.EnsureUser("NT AUTHORITY\\Authenticated Users");
                //SPRoleAssignment newRoleAssignmentToAdd = new SPRoleAssignment(allusers);
                //SPRoleDefinition _roleDefinition = web.RoleDefinitions.GetByType(SPRoleType.Reader);
                //newRoleAssignmentToAdd.RoleDefinitionBindings.Add(_roleDefinition);
                //VendorsList.RoleAssignments.Add(newRoleAssignmentToAdd);

                CardsList.Update();

                SPFieldText CategoryField = (SPFieldText)CardsList.Fields.GetFieldByInternalName("Title");
                CategoryField.StaticName = "CardNumber";
                CategoryField.Title = "Card Number";
                CategoryField.Update();

                //Card Type(Prepaid, Postpaid, Loyalty Card) *
                //Status is automatically displayed based on expiry date

                //CardHolder
                CardsList.Fields.Add("CardHolder", SPFieldType.User, true);
                SPFieldUser fieldCardHolder = (SPFieldUser)CardsList.Fields.GetFieldByInternalName("CardHolder");
                fieldCardHolder.AllowMultipleValues = false;
                fieldCardHolder.SelectionMode = SPFieldUserSelectionMode.PeopleOnly;
                fieldCardHolder.LookupField = "ImnName";
                fieldCardHolder.Title = "Card Holder";
                fieldCardHolder.Update();

                //Vendor
                CardsList.Fields.AddLookup("Vendor", vendorsList.ID, true);
                SPFieldLookup fieldVendor = (SPFieldLookup)CardsList.Fields.GetFieldByInternalName("Vendor");
                fieldVendor.Title = "Vendor";
                fieldVendor.LookupField = "Title";
                fieldVendor.LookupWebId = web.ID;
                fieldVendor.Indexed = true;
                fieldVendor.RelationshipDeleteBehavior = SPRelationshipDeleteBehavior.Restrict;
                fieldVendor.Update();

                //Issue Date
                CardsList.Fields.Add("IssueDate", SPFieldType.DateTime, true);
                SPFieldDateTime fieldIssueDate = (SPFieldDateTime)CardsList.Fields.GetFieldByInternalName("IssueDate");
                fieldIssueDate.Title = "Issue Date";
                fieldIssueDate.DisplayFormat = SPDateTimeFieldFormatType.DateOnly;
                fieldIssueDate.Update();

                //Expiry Date
                CardsList.Fields.Add("ExpiryDate", SPFieldType.DateTime, true);
                SPFieldDateTime fieldExpiryDate = (SPFieldDateTime)CardsList.Fields.GetFieldByInternalName("ExpiryDate");
                fieldExpiryDate.Title = "Expiry Date";
                fieldExpiryDate.DisplayFormat = SPDateTimeFieldFormatType.DateOnly;
                fieldExpiryDate.Update();

                //Card Type
                CardsList.Fields.Add("CardType", SPFieldType.Choice, true);
                SPFieldChoice fieldCardType = (SPFieldChoice)CardsList.Fields.GetFieldByInternalName("CardType");
                fieldCardType.EditFormat = SPChoiceFormatType.RadioButtons;
                fieldCardType.Choices.Add("Prepaid");
                fieldCardType.Choices.Add("Postpaid");
                fieldCardType.Choices.Add("Loyalty Card");
                //fieldCategory.DefaultValue = "Category 1";
                fieldCardType.Title = "Card Type";
                fieldCardType.Update();

                //make new columns visible in default view
                SPView view = CardsList.DefaultView;
                view.ViewFields.Add("CardHolder");
                view.ViewFields.Add("Vendor");
                view.ViewFields.Add("IssueDate");
                view.ViewFields.Add("ExpiryDate");
                view.ViewFields.Add("CardType");
                view.Query = "<OrderBy><FieldRef Name=\"ID\" Ascending=\"FALSE\" /></OrderBy>";
                view.Update();

                CardsList.ValidationFormula = "=[Issue Date]<=[Expiry Date]";
                CardsList.ValidationMessage = "Issue Date must be less than Expiry Date";
                CardsList.Update();

                //Granting permissions to security groups
                string AdminsGroupName = SPUtility.GetLocalizedString("$Resources:AdminsGroupName", "Club0803_Config", 1033);
                string SuperAdminsGroupName = SPUtility.GetLocalizedString("$Resources:SuperAdminsGroupName", "Club0803_Config", 1033);

                if (!CardsList.HasUniqueRoleAssignments)
                {
                    CardsList.BreakRoleInheritance(false);
                }

                SPGroup AdminsGroup = web.SiteGroups[AdminsGroupName];
                if (AdminsGroup != null)
                {
                    SPPrincipal AdminsGroupPrincipal = AdminsGroup as SPPrincipal;
                    SPRoleAssignment newRoleAssignmentToAdd = new SPRoleAssignment(AdminsGroupPrincipal);
                    SPRoleDefinition roleDefinition = web.RoleDefinitions.GetByType(SPRoleType.Contributor);
                    newRoleAssignmentToAdd.RoleDefinitionBindings.Add(roleDefinition);
                    CardsList.RoleAssignments.Add(newRoleAssignmentToAdd);
                }

                SPGroup SuperAdminsGroup = web.SiteGroups[SuperAdminsGroupName];
                if (SuperAdminsGroup != null)
                {
                    SPPrincipal SuperAdminsGroupPrincipal = SuperAdminsGroup as SPPrincipal;
                    SPRoleAssignment newRoleAssignmentToAdd = new SPRoleAssignment(SuperAdminsGroupPrincipal);
                    SPRoleDefinition roleDefinition = web.RoleDefinitions.GetByType(SPRoleType.Contributor);
                    newRoleAssignmentToAdd.RoleDefinitionBindings.Add(roleDefinition);
                    CardsList.RoleAssignments.Add(newRoleAssignmentToAdd);
                }

                CardsList.EventReceivers.Add(SPEventReceiverType.ItemAdded,
          "TAVIA.SharePoint.Club0803, Version=1.0.0.0, Culture=neutral, PublicKeyToken=e94667b04745ceb1",
                  "TAVIA.SharePoint.Club0803.EventHandlers.CardsListEventHandler");
                CardsList.Update();

            }
            return CardsList;
        }

        private SPList AddCardRequestsList(SPWeb web, SPList vendorsList)
        {
            string CardRequestsListName = SPUtility.GetLocalizedString("$Resources:CardRequestsListName", "Club0803_Config", 1033);
            string CardRequestsListInternalName = SPUtility.GetLocalizedString("$Resources:CardRequestsListUrl", "Club0803_Config", 1033);
            SPList CardRequestsList = null;
            try
            {
                string listUrl = String.Format("{0}/lists/{1}", web.Url, CardRequestsListInternalName);
                CardRequestsList = web.GetList(listUrl);
            }
            catch (FileNotFoundException e)
            {
                CardRequestsList = null;
            }

            if (CardRequestsList == null)
            {
                Guid listID = web.Lists.Add(CardRequestsListInternalName, String.Empty, SPListTemplateType.GenericList);
                CardRequestsList = web.Lists[listID];
                CardRequestsList.Title = CardRequestsListName;
                CardRequestsList.OnQuickLaunch = false;
                //VendorsList.DisableGridEditing = true;

                CardRequestsList.Update();

                //SPFieldText CategoryField = (SPFieldText)CardRequestsList.Fields.GetFieldByInternalName("Title");
                //CategoryField.StaticName = "RequestNumber";
                //CategoryField.Title = "Request Number";
                //CategoryField.Update();

                //Vendor
                CardRequestsList.Fields.AddLookup("Vendor", vendorsList.ID, true);
                SPFieldLookup fieldVendor = (SPFieldLookup)CardRequestsList.Fields.GetFieldByInternalName("Vendor");
                fieldVendor.Title = "Vendor";
                fieldVendor.LookupField = "Title";
                fieldVendor.LookupWebId = web.ID;
                fieldVendor.Indexed = true;
                fieldVendor.RelationshipDeleteBehavior = SPRelationshipDeleteBehavior.Restrict;
                fieldVendor.Update();

                //Employee Name(Automatically pick from current user)
                CardRequestsList.Fields.Add("EmployeeName", SPFieldType.User, true);
                SPFieldUser fieldEmployeeName = (SPFieldUser)CardRequestsList.Fields.GetFieldByInternalName("EmployeeName");
                fieldEmployeeName.AllowMultipleValues = false;
                fieldEmployeeName.SelectionMode = SPFieldUserSelectionMode.PeopleOnly;
                fieldEmployeeName.LookupField = "ImnName";
                fieldEmployeeName.Title = "Employee Name";
                fieldEmployeeName.Update();

                //Job Location(Lookup)
                SPList locationsList = web.Site.RootWeb.Lists["Locations"];

                CardRequestsList.Fields.AddLookup("JobLocation", locationsList.ID, true);
                SPFieldLookup locationField = (SPFieldLookup)CardRequestsList.Fields["JobLocation"];
                locationField.LookupField = locationsList.Fields["Location"].InternalName;
                locationField.LookupWebId = web.Site.RootWeb.ID;
                locationField.Title = "Job Location";
                locationField.Update();

                //Phone Number(Mandatory)
                CardRequestsList.Fields.Add("PhoneNumber", SPFieldType.Text, true);
                SPFieldText fieldPhoneNumber = (SPFieldText)CardRequestsList.Fields.GetFieldByInternalName("PhoneNumber");
                fieldPhoneNumber.Title = "Phone Number";
                fieldPhoneNumber.MaxLength = 13;
                fieldPhoneNumber.ValidationFormula = "=OR(AND(LEN([Phone Number])=11,ISNUMBER([Phone Number]+0)),AND(LEN([Phone Number])=13,ISNUMBER([Phone Number]+0)))";
                fieldPhoneNumber.ValidationMessage = "Please enter a valid phone number";
                fieldPhoneNumber.Update();

                //Date Required(Mandatory)
                CardRequestsList.Fields.Add("DateRequired", SPFieldType.DateTime, true);
                SPFieldDateTime fieldDateRequired = (SPFieldDateTime)CardRequestsList.Fields.GetFieldByInternalName("DateRequired");
                fieldDateRequired.Title = "Date Required";
                fieldDateRequired.DisplayFormat = SPDateTimeFieldFormatType.DateOnly;
                fieldDateRequired.Update();

                //Comments (Multiline / Optional)
                CardRequestsList.Fields.Add("Comments", SPFieldType.Note, false);
                SPFieldMultiLineText fieldComments = (SPFieldMultiLineText)CardRequestsList.Fields.GetFieldByInternalName("Comments");
                fieldComments.RichText = false;
                fieldComments.NumberOfLines = 6;
                fieldComments.Title = "Comments";
                fieldComments.Update();

                //make new columns visible in default view
                SPView view = CardRequestsList.DefaultView;
                view.ViewFields.Add("Vendor");
                view.ViewFields.Add("EmployeeName");
                view.ViewFields.Add("JobLocation");
                view.ViewFields.Add("PhoneNumber");
                view.ViewFields.Add("DateRequired");
                view.ViewFields.Add("Comments");
                view.Query = "<OrderBy><FieldRef Name=\"ID\" Ascending=\"FALSE\" /></OrderBy>";
                view.Update();

                //Granting permissions to security groups
                string AdminsGroupName = SPUtility.GetLocalizedString("$Resources:AdminsGroupName", "Club0803_Config", 1033);
                string SuperAdminsGroupName = SPUtility.GetLocalizedString("$Resources:SuperAdminsGroupName", "Club0803_Config", 1033);

                if (!CardRequestsList.HasUniqueRoleAssignments)
                {
                    CardRequestsList.BreakRoleInheritance(false);
                }

                SPGroup AdminsGroup = web.SiteGroups[AdminsGroupName];
                if (AdminsGroup != null)
                {
                    SPPrincipal AdminsGroupPrincipal = AdminsGroup as SPPrincipal;
                    SPRoleAssignment newRoleAssignmentToAdd = new SPRoleAssignment(AdminsGroupPrincipal);
                    SPRoleDefinition roleDefinition = web.RoleDefinitions.GetByType(SPRoleType.Reader);
                    newRoleAssignmentToAdd.RoleDefinitionBindings.Add(roleDefinition);
                    CardRequestsList.RoleAssignments.Add(newRoleAssignmentToAdd);
                }

                SPGroup SuperAdminsGroup = web.SiteGroups[SuperAdminsGroupName];
                if (SuperAdminsGroup != null)
                {
                    SPPrincipal SuperAdminsGroupPrincipal = SuperAdminsGroup as SPPrincipal;
                    SPRoleAssignment newRoleAssignmentToAdd = new SPRoleAssignment(SuperAdminsGroupPrincipal);
                    SPRoleDefinition roleDefinition = web.RoleDefinitions.GetByType(SPRoleType.Reader);
                    newRoleAssignmentToAdd.RoleDefinitionBindings.Add(roleDefinition);
                    CardRequestsList.RoleAssignments.Add(newRoleAssignmentToAdd);
                }

                CardRequestsList.EventReceivers.Add(SPEventReceiverType.ItemAdded,
"TAVIA.SharePoint.Club0803, Version=1.0.0.0, Culture=neutral, PublicKeyToken=e94667b04745ceb1",
  "TAVIA.SharePoint.Club0803.EventHandlers.CardRequestsListEventHandler");
                CardRequestsList.Update();
            }
            return CardRequestsList;
        }

        private SPList AddDiscountedItemsList(SPWeb web, SPFeatureReceiverProperties properties, SPList vendorsList, SPList categoriesList, SPList categoryTypesList)
        {
            string DiscountedItemsListName = SPUtility.GetLocalizedString("$Resources:DiscountedItemsListName", "Club0803_Config", 1033);
            string DiscountedItemsListInternalName = SPUtility.GetLocalizedString("$Resources:DiscountedItemsListUrl", "Club0803_Config", 1033);
            SPList DiscountedItemsList = null;
            try
            {
                string listUrl = String.Format("{0}/lists/{1}", web.Url, DiscountedItemsListInternalName);
                DiscountedItemsList = web.GetList(listUrl);
            }
            catch (FileNotFoundException e)
            {
                DiscountedItemsList = null;
            }

            //if (SecurityEscortRequestsList != null)
            //{
            //    SecurityEscortRequestsList.Delete();
            //    SecurityEscortRequestsList = null;
            //}

            if (DiscountedItemsList == null)
            {
                Guid listID = web.Lists.Add(DiscountedItemsListName, String.Empty, "Lists/" + DiscountedItemsListInternalName, properties.Feature.DefinitionId.ToString(), 13050, "101", SPListTemplate.QuickLaunchOptions.On);
                DiscountedItemsList = web.Lists[listID];
                DiscountedItemsList.DisableGridEditing = true;
                //DiscountedItemsList.BreakRoleInheritance(false);
                //SPUser allusers = web.EnsureUser("NT AUTHORITY\\Authenticated Users");
                //SPRoleAssignment newRoleAssignmentToAdd = new SPRoleAssignment(allusers);
                //SPRoleDefinition _roleDefinition = web.RoleDefinitions.GetByType(SPRoleType.Contributor);
                //newRoleAssignmentToAdd.RoleDefinitionBindings.Add(_roleDefinition);
                //DiscountedItemsList.RoleAssignments.Add(newRoleAssignmentToAdd);
                //DiscountedItemsList.Update();

                //SPList divisionsList = web.Site.RootWeb.Lists["Divisions"];

                DiscountedItemsList.Fields.AddLookup("Category", categoriesList.ID, true);
                SPFieldLookup categoryField = (SPFieldLookup)DiscountedItemsList.Fields["Category"];
                categoryField.LookupField = categoriesList.Fields["Category"].InternalName;
                //categoryField.LookupWebId = web.Site.RootWeb.ID;
                categoryField.Update();

                DiscountedItemsList.Fields.AddLookup("CategoryType", categoryTypesList.ID, true);
                SPFieldLookup categoryTypeField = (SPFieldLookup)DiscountedItemsList.Fields["CategoryType"];
                categoryTypeField.LookupField = categoryTypesList.Fields["Category Type"].InternalName;
                //categoryTypeField.LookupWebId = web.Site.RootWeb.ID;
                categoryTypeField.Title = "Category Type";
                categoryTypeField.Update();

                DiscountedItemsList.Fields.AddLookup("VendorName", vendorsList.ID, true);
                SPFieldLookup vendorNameField = (SPFieldLookup)DiscountedItemsList.Fields["VendorName"];
                vendorNameField.LookupField = vendorsList.Fields["Vendor Name"].InternalName;
                //vendorNameField.LookupWebId = web.Site.RootWeb.ID;
                vendorNameField.Title = "Vendor Name";
                vendorNameField.Update();

                SPView view = DiscountedItemsList.DefaultView;
                view.ViewFields.Add("Author");
                view.ViewFields.Add("Created");
                view.ViewFields.Add("Category");
                view.ViewFields.Add("CategoryType");
                view.ViewFields.Add("VendorName");
                view.Update();

                StringCollection colViewFields1 = new StringCollection();
                colViewFields1.Add("Author");
                colViewFields1.Add("Created");
                colViewFields1.Add("Category");
                colViewFields1.Add("CategoryType");
                colViewFields1.Add("VendorName");
                string viewQuery1 = "<OrderBy><FieldRef Name=\"ID\" Ascending=\"FALSE\" /></OrderBy>";
                DiscountedItemsList.Views.Add("ProductsReport", colViewFields1, viewQuery1, 100, true, false);

                StringCollection colViewFields2 = new StringCollection();
                colViewFields2.Add("Author");
                colViewFields2.Add("Created");
                colViewFields2.Add("Category");
                colViewFields2.Add("CategoryType");
                colViewFields2.Add("VendorName");

                string viewQuery2 = "<OrderBy><FieldRef Name=\"ID\" Ascending=\"FALSE\" /></OrderBy>";
                DiscountedItemsList.Views.Add("BenefitsReport", colViewFields2, viewQuery2, 100, true, false);

                string rootWebUrl = web.Site.Url;
                CloseAllWebParts(web, rootWebUrl + DiscountedItemsList.DefaultDisplayFormUrl);
                CloseAllWebParts(web, rootWebUrl + DiscountedItemsList.DefaultEditFormUrl);
                CloseAllWebParts(web, rootWebUrl + DiscountedItemsList.DefaultNewFormUrl);

                //Granting permissions to security groups
                string AdminsGroupName = SPUtility.GetLocalizedString("$Resources:AdminsGroupName", "Club0803_Config", 1033);
                string SuperAdminsGroupName = SPUtility.GetLocalizedString("$Resources:SuperAdminsGroupName", "Club0803_Config", 1033);
                string PermanentStaffGroupName = SPUtility.GetLocalizedString("$Resources:PermanentStaffGroupName", "Club0803_Config", 1033);

                if (!DiscountedItemsList.HasUniqueRoleAssignments)
                {
                    DiscountedItemsList.BreakRoleInheritance(false);
                }

                SPGroup AdminsGroup = web.SiteGroups[AdminsGroupName];
                if (AdminsGroup != null)
                {
                    SPPrincipal AdminsGroupPrincipal = AdminsGroup as SPPrincipal;
                    SPRoleAssignment newRoleAssignmentToAdd = new SPRoleAssignment(AdminsGroupPrincipal);
                    SPRoleDefinition roleDefinition = web.RoleDefinitions.GetByType(SPRoleType.Contributor);
                    newRoleAssignmentToAdd.RoleDefinitionBindings.Add(roleDefinition);
                    DiscountedItemsList.RoleAssignments.Add(newRoleAssignmentToAdd);
                }

                SPGroup SuperAdminsGroup = web.SiteGroups[SuperAdminsGroupName];
                if (SuperAdminsGroup != null)
                {
                    SPPrincipal SuperAdminsGroupPrincipal = SuperAdminsGroup as SPPrincipal;
                    SPRoleAssignment newRoleAssignmentToAdd = new SPRoleAssignment(SuperAdminsGroupPrincipal);
                    SPRoleDefinition roleDefinition = web.RoleDefinitions.GetByType(SPRoleType.Contributor);
                    newRoleAssignmentToAdd.RoleDefinitionBindings.Add(roleDefinition);
                    DiscountedItemsList.RoleAssignments.Add(newRoleAssignmentToAdd);
                }

                SPGroup PermanentStaffGroup = web.SiteGroups[PermanentStaffGroupName];
                if (PermanentStaffGroup != null)
                {
                    SPPrincipal PermanentStaffGroupPrincipal = PermanentStaffGroup as SPPrincipal;
                    SPRoleAssignment newRoleAssignmentToAdd = new SPRoleAssignment(PermanentStaffGroupPrincipal);
                    SPRoleDefinition roleDefinition = web.RoleDefinitions.GetByType(SPRoleType.Reader);
                    newRoleAssignmentToAdd.RoleDefinitionBindings.Add(roleDefinition);
                    DiscountedItemsList.RoleAssignments.Add(newRoleAssignmentToAdd);
                }
            }

            return DiscountedItemsList;
        }

        private SPList AddOrdersList(SPWeb web, SPList discountedItemsList)
        {
            string OrdersListName = SPUtility.GetLocalizedString("$Resources:OrdersListName", "Club0803_Config", 1033);
            string OrdersListInternalName = SPUtility.GetLocalizedString("$Resources:OrdersListUrl", "Club0803_Config", 1033);
            SPList OrdersList = null;
            try
            {
                string listUrl = String.Format("{0}/lists/{1}", web.Url, OrdersListInternalName);
                OrdersList = web.GetList(listUrl);
            }
            catch (FileNotFoundException e)
            {
                OrdersList = null;
            }

            if (OrdersList == null)
            {
                Guid listID = web.Lists.Add(OrdersListInternalName, String.Empty, SPListTemplateType.GenericList);
                OrdersList = web.Lists[listID];
                OrdersList.Title = OrdersListName;
                OrdersList.OnQuickLaunch = false;
                //VendorsList.DisableGridEditing = true;

                OrdersList.Update();

                //SPFieldText CategoryField = (SPFieldText)CardRequestsList.Fields.GetFieldByInternalName("Title");
                //CategoryField.StaticName = "RequestNumber";
                //CategoryField.Title = "Request Number";
                //CategoryField.Update();

                //DiscountedItem
                OrdersList.Fields.AddLookup("DiscountedItem", discountedItemsList.ID, true);
                SPFieldLookup fieldDiscountedItem = (SPFieldLookup)OrdersList.Fields.GetFieldByInternalName("DiscountedItem");
                fieldDiscountedItem.Title = "Discounted Item";
                fieldDiscountedItem.LookupField = "Title";
                fieldDiscountedItem.LookupWebId = web.ID;
                fieldDiscountedItem.Indexed = true;
                fieldDiscountedItem.RelationshipDeleteBehavior = SPRelationshipDeleteBehavior.Restrict;
                fieldDiscountedItem.Update();

                //Employee Name(Automatically pick from current user)
                OrdersList.Fields.Add("EmployeeName", SPFieldType.User, true);
                SPFieldUser fieldEmployeeName = (SPFieldUser)OrdersList.Fields.GetFieldByInternalName("EmployeeName");
                fieldEmployeeName.AllowMultipleValues = false;
                fieldEmployeeName.SelectionMode = SPFieldUserSelectionMode.PeopleOnly;
                fieldEmployeeName.LookupField = "ImnName";
                fieldEmployeeName.Title = "Employee Name";
                fieldEmployeeName.Update();

                //DiscountedItemRating
                OrdersList.Fields.Add("DiscountedItemRating", SPFieldType.Number, false);
                SPFieldNumber fieldDiscountedItemRating = (SPFieldNumber)OrdersList.Fields.GetFieldByInternalName("DiscountedItemRating");
                fieldDiscountedItemRating.Title = "Discounted Item Rating";
                fieldDiscountedItemRating.MinimumValue = 1;
                fieldDiscountedItemRating.MaximumValue = 5;
                fieldDiscountedItemRating.DisplayFormat = SPNumberFormatTypes.NoDecimal;
                fieldDiscountedItemRating.Update();

                //VendorResponseRating
                OrdersList.Fields.Add("VendorResponseRating", SPFieldType.Number, false);
                SPFieldNumber fieldVendorResponseRating = (SPFieldNumber)OrdersList.Fields.GetFieldByInternalName("VendorResponseRating");
                fieldVendorResponseRating.Title = "Vendor Response Rating";
                fieldVendorResponseRating.MinimumValue = 1;
                fieldVendorResponseRating.MaximumValue = 5;
                fieldVendorResponseRating.DisplayFormat = SPNumberFormatTypes.NoDecimal;
                fieldVendorResponseRating.Update();

                //VendorComplaintsRating
                OrdersList.Fields.Add("VendorComplaintsRating", SPFieldType.Number, false);
                SPFieldNumber fieldVendorComplaintsRating = (SPFieldNumber)OrdersList.Fields.GetFieldByInternalName("VendorComplaintsRating");
                fieldVendorComplaintsRating.Title = "Vendor Complaints Satisfaction Rating";
                fieldVendorComplaintsRating.MinimumValue = 1;
                fieldVendorComplaintsRating.MaximumValue = 5;
                fieldVendorComplaintsRating.DisplayFormat = SPNumberFormatTypes.NoDecimal;
                fieldVendorComplaintsRating.Update();

                //DiscountedItemID
                OrdersList.Fields.Add("DiscountedItemID", SPFieldType.Number, false);
                SPFieldNumber fieldDiscountedItemID = (SPFieldNumber)OrdersList.Fields.GetFieldByInternalName("DiscountedItemID");
                fieldDiscountedItemID.Title = "DiscountedItemID";
                fieldDiscountedItemID.DisplayFormat = SPNumberFormatTypes.NoDecimal;
                fieldDiscountedItemID.Update();

                //Vendor
                OrdersList.Fields.Add("Vendor", SPFieldType.Text, false);
                SPFieldText fieldVendor = (SPFieldText)OrdersList.Fields.GetFieldByInternalName("Vendor");
                fieldVendor.Title = "Vendor";
                fieldVendor.Update();

                //make new columns visible in default view
                SPView view = OrdersList.DefaultView;
                view.ViewFields.Add("DiscountedItem");
                view.ViewFields.Add("EmployeeName");
                view.ViewFields.Add("Created");
                view.ViewFields.Add("DiscountedItemRating");
                view.ViewFields.Add("VendorResponseRating");
                view.ViewFields.Add("VendorComplaintsRating");
                view.Query = "<OrderBy><FieldRef Name=\"ID\" Ascending=\"FALSE\" /></OrderBy>";
                view.Update();

                StringCollection colViewFields = new StringCollection();
                colViewFields.Add("DiscountedItem");
                colViewFields.Add("EmployeeName");
                colViewFields.Add("Created");
                colViewFields.Add("DiscountedItemRating");
                colViewFields.Add("VendorResponseRating");
                colViewFields.Add("VendorComplaintsRating");
                string viewQuery = "<GroupBy Collapse=\"TRUE\" GroupLimit=\"30\"><FieldRef Name=\"DiscountedItem\" /></GroupBy><OrderBy><FieldRef Name=\"ID\" Ascending=\"FALSE\" /></OrderBy>";
                OrdersList.Views.Add("Report", colViewFields, viewQuery, 100, true, false);

                //Granting permissions to security groups
                string AdminsGroupName = SPUtility.GetLocalizedString("$Resources:AdminsGroupName", "Club0803_Config", 1033);
                string SuperAdminsGroupName = SPUtility.GetLocalizedString("$Resources:SuperAdminsGroupName", "Club0803_Config", 1033);

                if (!OrdersList.HasUniqueRoleAssignments)
                {
                    OrdersList.BreakRoleInheritance(false);
                }

                SPGroup AdminsGroup = web.SiteGroups[AdminsGroupName];
                if (AdminsGroup != null)
                {
                    SPPrincipal AdminsGroupPrincipal = AdminsGroup as SPPrincipal;
                    SPRoleAssignment newRoleAssignmentToAdd = new SPRoleAssignment(AdminsGroupPrincipal);
                    SPRoleDefinition roleDefinition = web.RoleDefinitions.GetByType(SPRoleType.Reader);
                    newRoleAssignmentToAdd.RoleDefinitionBindings.Add(roleDefinition);
                    OrdersList.RoleAssignments.Add(newRoleAssignmentToAdd);
                }

                SPGroup SuperAdminsGroup = web.SiteGroups[SuperAdminsGroupName];
                if (SuperAdminsGroup != null)
                {
                    SPPrincipal SuperAdminsGroupPrincipal = SuperAdminsGroup as SPPrincipal;
                    SPRoleAssignment newRoleAssignmentToAdd = new SPRoleAssignment(SuperAdminsGroupPrincipal);
                    SPRoleDefinition roleDefinition = web.RoleDefinitions.GetByType(SPRoleType.Reader);
                    newRoleAssignmentToAdd.RoleDefinitionBindings.Add(roleDefinition);
                    OrdersList.RoleAssignments.Add(newRoleAssignmentToAdd);
                }

                OrdersList.EventReceivers.Add(SPEventReceiverType.ItemAdded,
                          "TAVIA.SharePoint.Club0803, Version=1.0.0.0, Culture=neutral, PublicKeyToken=e94667b04745ceb1",
                                  "TAVIA.SharePoint.Club0803.EventHandlers.OrdersListEventHandler");
                OrdersList.Update();

            }

            return OrdersList;
        }

        private SPList AddOrdersCartList(SPWeb web, SPList discountedItemsList)
        {
            string OrdersCartListName = SPUtility.GetLocalizedString("$Resources:OrdersCartListName", "Club0803_Config", 1033);
            string OrdersCartListInternalName = SPUtility.GetLocalizedString("$Resources:OrdersCartListUrl", "Club0803_Config", 1033);
            SPList OrdersCartList = null;
            try
            {
                string listUrl = String.Format("{0}/lists/{1}", web.Url, OrdersCartListInternalName);
                OrdersCartList = web.GetList(listUrl);
            }
            catch (FileNotFoundException e)
            {
                OrdersCartList = null;
            }

            if (OrdersCartList == null)
            {
                Guid listID = web.Lists.Add(OrdersCartListInternalName, String.Empty, SPListTemplateType.GenericList);
                OrdersCartList = web.Lists[listID];
                OrdersCartList.Title = OrdersCartListName;
                OrdersCartList.OnQuickLaunch = false;
                //VendorsList.DisableGridEditing = true;

                OrdersCartList.Update();

                //SPFieldText CategoryField = (SPFieldText)CardRequestsList.Fields.GetFieldByInternalName("Title");
                //CategoryField.StaticName = "RequestNumber";
                //CategoryField.Title = "Request Number";
                //CategoryField.Update();

                //DiscountedItem
                OrdersCartList.Fields.AddLookup("DiscountedItem", discountedItemsList.ID, true);
                SPFieldLookup fieldDiscountedItem = (SPFieldLookup)OrdersCartList.Fields.GetFieldByInternalName("DiscountedItem");
                fieldDiscountedItem.Title = "Discounted Item";
                fieldDiscountedItem.LookupField = "Title";
                fieldDiscountedItem.LookupWebId = web.ID;
                fieldDiscountedItem.Indexed = true;
                fieldDiscountedItem.RelationshipDeleteBehavior = SPRelationshipDeleteBehavior.Restrict;
                fieldDiscountedItem.Update();

                //Employee Name(Automatically pick from current user)
                OrdersCartList.Fields.Add("EmployeeName", SPFieldType.User, true);
                SPFieldUser fieldEmployeeName = (SPFieldUser)OrdersCartList.Fields.GetFieldByInternalName("EmployeeName");
                fieldEmployeeName.AllowMultipleValues = false;
                fieldEmployeeName.SelectionMode = SPFieldUserSelectionMode.PeopleOnly;
                fieldEmployeeName.LookupField = "ImnName";
                fieldEmployeeName.Title = "Employee Name";
                fieldEmployeeName.Update();

                //DiscountedItemID
                OrdersCartList.Fields.Add("DiscountedItemID", SPFieldType.Number, false);
                SPFieldNumber fieldDiscountedItemID = (SPFieldNumber)OrdersCartList.Fields.GetFieldByInternalName("DiscountedItemID");
                fieldDiscountedItemID.Title = "DiscountedItemID";
                fieldDiscountedItemID.DisplayFormat = SPNumberFormatTypes.NoDecimal;
                fieldDiscountedItemID.Update();

                //Vendor
                OrdersCartList.Fields.Add("Vendor", SPFieldType.Text, false);
                SPFieldText fieldVendor = (SPFieldText)OrdersCartList.Fields.GetFieldByInternalName("Vendor");
                fieldVendor.Title = "Vendor";
                fieldVendor.Update();

                //ImgUrl
                OrdersCartList.Fields.Add("ImgUrl", SPFieldType.Text, false);
                SPFieldText fieldImgUrl = (SPFieldText)OrdersCartList.Fields.GetFieldByInternalName("ImgUrl");
                fieldImgUrl.Title = "ImgUrl";
                fieldImgUrl.Update();

                //make new columns visible in default view
                SPView view = OrdersCartList.DefaultView;
                view.ViewFields.Add("DiscountedItem");
                view.ViewFields.Add("Vendor");
                view.ViewFields.Add("EmployeeName");
                view.Query = "<OrderBy><FieldRef Name=\"ID\" Ascending=\"FALSE\" /></OrderBy>";
                view.Update();

                //Granting permissions to security groups
                string AdminsGroupName = SPUtility.GetLocalizedString("$Resources:AdminsGroupName", "Club0803_Config", 1033);
                string SuperAdminsGroupName = SPUtility.GetLocalizedString("$Resources:SuperAdminsGroupName", "Club0803_Config", 1033);

                if (!OrdersCartList.HasUniqueRoleAssignments)
                {
                    OrdersCartList.BreakRoleInheritance(false);
                }

                SPGroup AdminsGroup = web.SiteGroups[AdminsGroupName];
                if (AdminsGroup != null)
                {
                    SPPrincipal AdminsGroupPrincipal = AdminsGroup as SPPrincipal;
                    SPRoleAssignment newRoleAssignmentToAdd = new SPRoleAssignment(AdminsGroupPrincipal);
                    SPRoleDefinition roleDefinition = web.RoleDefinitions.GetByType(SPRoleType.Reader);
                    newRoleAssignmentToAdd.RoleDefinitionBindings.Add(roleDefinition);
                    OrdersCartList.RoleAssignments.Add(newRoleAssignmentToAdd);
                }

                SPGroup SuperAdminsGroup = web.SiteGroups[SuperAdminsGroupName];
                if (SuperAdminsGroup != null)
                {
                    SPPrincipal SuperAdminsGroupPrincipal = SuperAdminsGroup as SPPrincipal;
                    SPRoleAssignment newRoleAssignmentToAdd = new SPRoleAssignment(SuperAdminsGroupPrincipal);
                    SPRoleDefinition roleDefinition = web.RoleDefinitions.GetByType(SPRoleType.Reader);
                    newRoleAssignmentToAdd.RoleDefinitionBindings.Add(roleDefinition);
                    OrdersCartList.RoleAssignments.Add(newRoleAssignmentToAdd);
                }
            }
            return OrdersCartList;
        }

        private void AddActorsGroups(SPWeb web)
        {
            //Adding Actors Related Groups in case they are not there
            string AdminsGroupName = SPUtility.GetLocalizedString("$Resources:AdminsGroupName", "Club0803_Config", 1033);
            string SuperAdminsGroupName = SPUtility.GetLocalizedString("$Resources:SuperAdminsGroupName", "Club0803_Config", 1033);
            string PermanentStaffGroupName = SPUtility.GetLocalizedString("$Resources:PermanentStaffGroupName", "Club0803_Config", 1033);

            SPUser currentUser = web.CurrentUser;
            SPMember currentMember = (SPMember)currentUser;
            try
            {
                SPGroup tempGroup = web.SiteGroups[AdminsGroupName];
            }
            catch
            {
                web.SiteGroups.Add(AdminsGroupName, currentMember, currentUser, "");
                SPGroup tempGroup = web.SiteGroups[AdminsGroupName];
                tempGroup.OnlyAllowMembersViewMembership = false;
                tempGroup.Update();
            }

            try
            {
                SPGroup tempGroup = web.SiteGroups[SuperAdminsGroupName];
            }
            catch
            {
                web.SiteGroups.Add(SuperAdminsGroupName, currentMember, currentUser, "");
                SPGroup tempGroup = web.SiteGroups[SuperAdminsGroupName];
                tempGroup.OnlyAllowMembersViewMembership = false;
                tempGroup.Update();
            }

            try
            {
                SPGroup tempGroup = web.SiteGroups[PermanentStaffGroupName];
            }
            catch
            {
                web.SiteGroups.Add(PermanentStaffGroupName, currentMember, currentUser, "");
                SPGroup tempGroup = web.SiteGroups[PermanentStaffGroupName];
                tempGroup.OnlyAllowMembersViewMembership = false;
                tempGroup.Update();
            }

            //try
            //{
            //    SPGroup tempGroup = web.SiteGroups[NetworkSourcingGroupName];
            //}
            //catch
            //{
            //    web.SiteGroups.Add(NetworkSourcingGroupName, currentMember, currentUser, "");
            //    SPGroup tempGroup = web.SiteGroups[NetworkSourcingGroupName];
            //    tempGroup.OnlyAllowMembersViewMembership = false;
            //    tempGroup.Update();
            //}

            //try
            //{
            //    SPGroup tempGroup = web.SiteGroups[ITSourcingGroupName];
            //}
            //catch
            //{
            //    web.SiteGroups.Add(ITSourcingGroupName, currentMember, currentUser, "");
            //    SPGroup tempGroup = web.SiteGroups[ITSourcingGroupName];
            //    tempGroup.OnlyAllowMembersViewMembership = false;
            //    tempGroup.Update();
            //}
        }

        private void CloseAllWebParts(SPWeb web, string pageRealUrl)
        {
            try
            {
                SPFile file = web.GetFile(pageRealUrl);
                using (Microsoft.SharePoint.WebPartPages.SPLimitedWebPartManager webPartManager = file.GetLimitedWebPartManager(System.Web.UI.WebControls.WebParts.PersonalizationScope.Shared))
                {
                    int count = webPartManager.WebParts.Count;
                    foreach (Microsoft.SharePoint.WebPartPages.WebPart webpart in webPartManager.WebParts)
                    {
                        webPartManager.CloseWebPart(webpart);
                        webPartManager.SaveChanges(webpart);
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}
