﻿using System;
using System.Web;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using Microsoft.SharePoint.WebControls;
using Microsoft.SharePoint.WebPartPages;
using TAVIA.SharePoint.Utilities;

namespace TAVIA.SharePoint.Club0803.SitePagesModule
{
    public partial class NewVendorSuggestion : WebPartPage
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                SPRibbon ribbon = SPRibbon.GetCurrent(this.Page);
                if (ribbon != null)
                {
                    ribbon.Visible = false;
                }
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            GoBack();
        }

        private void GoBack()
        {
            if (Page.Request.QueryString["IsDlg"] != null)
            {
                HttpContext context = HttpContext.Current;
                context.Response.Write("<script type='text/javascript'>window.frameElement.cancelPopUp()</script>");
                context.Response.Flush();
                context.Response.End();
            }
            else if (!String.IsNullOrEmpty(Page.Request.QueryString["Source"]))
            {
                Response.Redirect(Page.Request.QueryString["Source"].ToString());
            }
            else
            {
                Response.Redirect(SPContext.Current.Web.Url);
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid)
                return;

            SPUser currentUser = SPContext.Current.Web.CurrentUser;

            SPSecurity.RunWithElevatedPrivileges(delegate ()
            {
                using (SPSite impersonatedSite = new SPSite(SPContext.Current.Web.Url))
                {
                    using (SPWeb impersonatedWeb = impersonatedSite.OpenWeb())
                    {
                        impersonatedWeb.AllowUnsafeUpdates = true;

                        string VendorSuggestionsListInternalName = SPUtility.GetLocalizedString("$Resources:VendorSuggestionsListUrl", "Club0803_Config", 1033);
                        string listUrl = String.Format("{0}/lists/{1}", impersonatedWeb.Url, VendorSuggestionsListInternalName);
                        SPList VendorSuggestionsList = impersonatedWeb.GetList(listUrl);

                        SPListItem item = VendorSuggestionsList.AddItem();

                        item["VendorName"] = SPHandler.RemoveHTMLTags(txtVendorName.Text);

                        if (!String.IsNullOrEmpty(txtPhoneNumber.Text))
                            item["PhoneNumber"] = SPHandler.RemoveHTMLTags(txtPhoneNumber.Text);

                        if (!String.IsNullOrEmpty(txtVendorWebsiteURL.Text))
                            item["WebsiteURL"] = SPHandler.RemoveHTMLTags(txtVendorWebsiteURL.Text);

                        if (!String.IsNullOrEmpty(txtVendorLocation.Text))
                            item["VendorLocation"] = SPHandler.RemoveHTMLTags(txtVendorLocation.Text);

                        if (!String.IsNullOrEmpty(txtComments.Text))
                            item["Comments"] = SPHandler.RemoveHTMLTags(txtComments.Text);


                        item["EmployeeName"] = new SPFieldUserValue(impersonatedWeb, currentUser.ID, currentUser.LoginName);

                        item.Update();

                        impersonatedWeb.AllowUnsafeUpdates = false;
                    }
                }
            });

            Response.Redirect("Info.aspx?msg=" + (int)Utility.Messages.Vendor_Suggestion_Submitted_Successfully);
        }


    }
}
