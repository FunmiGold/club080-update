﻿using System;
using System.Collections.Specialized;
using System.Data;
using System.Web.UI.WebControls;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using Microsoft.SharePoint.WebControls;
using Microsoft.SharePoint.WebPartPages;
using TAVIA.SharePoint.Club0803.Utility;
using TAVIA.SharePoint.Utilities;

namespace TAVIA.SharePoint.Club0803.SitePagesModule
{
    public partial class MyCart : WebPartPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                SPRibbon ribbon = SPRibbon.GetCurrent(this.Page);
                if (ribbon != null)
                {
                    ribbon.Visible = false;
                }
                SPUser currentUser = SPContext.Current.Web.CurrentUser;

                cbxConfirmation.Text = String.Format("&nbsp;I have read the <a target='_blank' href='{0}'>terms and conditions</a> and agree to them", SPUtility.GetLocalizedString("$Resources:Links_TermsAndConditions", "Club0803_Config", 1033));

                SPSecurity.RunWithElevatedPrivileges(delegate ()
                {
                    using (SPSite impersonatedSite = new SPSite(SPContext.Current.Web.Url))
                    {
                        using (SPWeb impersonatedWeb = impersonatedSite.OpenWeb())
                        {
                            impersonatedWeb.AllowUnsafeUpdates = true;

                            string DiscountedItemsListInternalName = SPUtility.GetLocalizedString("$Resources:DiscountedItemsListUrl", "Club0803_Config", 1033);
                            string DiscountedItemsListUrl = String.Format("{0}/lists/{1}", SPContext.Current.Web.Url, DiscountedItemsListInternalName);
                            SPList DiscountedItemsList = SPContext.Current.Web.GetList(DiscountedItemsListUrl);

                            string OrdersCartListInternalName = SPUtility.GetLocalizedString("$Resources:OrdersCartListUrl", "Club0803_Config", 1033);
                            string OrdersCartListtUrl = String.Format("{0}/lists/{1}", impersonatedWeb.Url, OrdersCartListInternalName);
                            SPList OrdersCartList = impersonatedWeb.GetList(OrdersCartListtUrl);

                            SPQuery query = new SPQuery();
                            query.Query = String.Format("<OrderBy><FieldRef Name=\"ID\" Ascending=\"FALSE\" /></OrderBy><Where><Eq><FieldRef Name=\"EmployeeName\" LookupId=\"TRUE\" /><Value Type=\"Lookup\">{0}</Value></Eq></Where>", currentUser.ID);
                            query.ViewFields = "<FieldRef Name=\"ID\" /><FieldRef Name=\"DiscountedItem\" /><FieldRef Name=\"EmployeeName\" /><FieldRef Name=\"DiscountedItemID\" /><FieldRef Name=\"Vendor\" /><FieldRef Name=\"ImgUrl\" />";
                            query.ViewFieldsOnly = true;
                            query.RowLimit = 1000;

                            repCartItems.DataSource = OrdersCartList.GetItems(query).GetDataTable();//tblAllItems;
                            repCartItems.DataBind();

                            impersonatedWeb.AllowUnsafeUpdates = false;
                        }
                    }
                });
            }
        }

        protected void repCartItems_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "DeleteItem")
            {
                int itemId;
                if (int.TryParse(e.CommandArgument.ToString(), out itemId))
                {
                    SPSecurity.RunWithElevatedPrivileges(delegate ()
                    {
                        using (SPSite impersonatedSite = new SPSite(SPContext.Current.Web.Url))
                        {
                            using (SPWeb impersonatedWeb = impersonatedSite.OpenWeb())
                            {
                                impersonatedWeb.AllowUnsafeUpdates = true;

                                string OrdersCartListInternalName = SPUtility.GetLocalizedString("$Resources:OrdersCartListUrl", "Club0803_Config", 1033);
                                string OrdersCartListUrl = String.Format("{0}/lists/{1}", impersonatedWeb.Url, OrdersCartListInternalName);
                                SPList OrdersCartList = impersonatedWeb.GetList(OrdersCartListUrl);

                                SPListItem ordersCartItem = OrdersCartList.GetItemById(itemId);
                                ordersCartItem.Delete();

                                //Hide the deleted row
                                e.Item.Visible = false;

                                impersonatedWeb.AllowUnsafeUpdates = false;
                            }
                        }
                    });
                }
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("ViewDiscountedItems.aspx");
        }

        private void GoBack()
        {
            //if (Page.Request.QueryString["IsDlg"] != null)
            //{
            //    HttpContext context = HttpContext.Current;
            //    context.Response.Write("<script type='text/javascript'>window.frameElement.cancelPopUp()</script>");
            //    context.Response.Flush();
            //    context.Response.End();
            //}
            //else if (!String.IsNullOrEmpty(Page.Request.QueryString["Source"]))
            //{
            //    Response.Redirect(Page.Request.QueryString["Source"].ToString());
            //}
            //else
            //{
            //    string DiscountedItemsListInternalName = SPUtility.GetLocalizedString("$Resources:DiscountedItemsListUrl", "Club0803_Config", 1033);
            //    string url = String.Format("{0}/lists/{1}", SPContext.Current.Web.Url, DiscountedItemsListInternalName);
            //    Response.Redirect(url);
            //    //Response.Redirect(SPContext.Current.Web.Url);
            //}
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid)
                return;
            SPUser currentUser = SPContext.Current.Web.CurrentUser;
            string memberName = currentUser.Name;
            string memberEmailAddress = String.Empty;
            if (!String.IsNullOrEmpty(currentUser.Email))
                memberEmailAddress = currentUser.Email;

            SPSecurity.RunWithElevatedPrivileges(delegate ()
            {
                using (SPSite impersonatedSite = new SPSite(SPContext.Current.Web.Url))
                {
                    using (SPWeb impersonatedWeb = impersonatedSite.OpenWeb())
                    {
                        impersonatedWeb.AllowUnsafeUpdates = true;

                        string VendorsListInternalName = SPUtility.GetLocalizedString("$Resources:VendorsListUrl", "Club0803_Config", 1033);
                        string VendorsListUrl = String.Format("{0}/lists/{1}", impersonatedWeb.Url, VendorsListInternalName);
                        SPList VendorsList = impersonatedWeb.GetList(VendorsListUrl);

                        string OrdersListInternalName = SPUtility.GetLocalizedString("$Resources:OrdersListUrl", "Club0803_Config", 1033);
                        string OrdersListUrl = String.Format("{0}/lists/{1}", impersonatedWeb.Url, OrdersListInternalName);
                        SPList OrdersList = impersonatedWeb.GetList(OrdersListUrl);

                        string OrdersCartListInternalName = SPUtility.GetLocalizedString("$Resources:OrdersCartListUrl", "Club0803_Config", 1033);
                        string OrdersCartListtUrl = String.Format("{0}/lists/{1}", impersonatedWeb.Url, OrdersCartListInternalName);
                        SPList OrdersCartList = impersonatedWeb.GetList(OrdersCartListtUrl);

                        string DiscountedItemsListInternalName = SPUtility.GetLocalizedString("$Resources:DiscountedItemsListUrl", "Club0803_Config", 1033);
                        string DiscountedItemsListUrl = String.Format("{0}/lists/{1}", impersonatedWeb.Url, DiscountedItemsListInternalName);
                        SPList DiscountedItemsList = impersonatedWeb.GetList(DiscountedItemsListUrl); ;

                        SPQuery query = new SPQuery();
                        query.Query = String.Format("<OrderBy><FieldRef Name=\"ID\" Ascending=\"FALSE\" /></OrderBy><Where><Eq><FieldRef Name=\"EmployeeName\" LookupId=\"TRUE\" /><Value Type=\"Lookup\">{0}</Value></Eq></Where>", currentUser.ID);
                        query.ViewFields = "<FieldRef Name=\"ID\" /><FieldRef Name=\"DiscountedItem\" /><FieldRef Name=\"EmployeeName\" /><FieldRef Name=\"Vendor\" />";
                        query.ViewFieldsOnly = true;
                        query.RowLimit = 1000;

                        SPListItemCollection colOrdersCartItems = OrdersCartList.GetItems(query);

                        foreach (SPListItem item in colOrdersCartItems)
                        {
                            SPFieldLookupValue DiscountedItemValue = new SPFieldLookupValue(item["DiscountedItem"].ToString());

                            SPListItem orderItem = OrdersList.AddItem();

                            SPFieldUserValue UserNameUserValue = new SPFieldUserValue(impersonatedWeb, currentUser.ID, currentUser.LoginName);

                            orderItem["DiscountedItem"] = DiscountedItemValue;
                            orderItem["EmployeeName"] = UserNameUserValue;
                            orderItem["DiscountedItemID"] = DiscountedItemValue.LookupId;
                            orderItem["Vendor"] = item["Vendor"];

                            orderItem.Update();

                            SPListItem discountedItem = DiscountedItemsList.GetItemById(DiscountedItemValue.LookupId);

                            SPFieldLookupValue VendorItemValue = new SPFieldLookupValue(discountedItem["VendorName"].ToString());
                            SPListItem vendorItem = VendorsList.GetItemById(VendorItemValue.LookupId);
                            string vendorName = String.Empty;
                            string vendorEmail = String.Empty;
                            string vendorPhoneNumber = String.Empty;

                            if (vendorItem["Title"] != null)
                            {
                                vendorName = vendorItem["Title"].ToString();
                            }
                            if (vendorItem["Email"] != null)
                            {
                                vendorEmail = vendorItem["Email"].ToString();
                            }
                            if (vendorItem["PhoneNumber"] != null)
                            {
                                vendorPhoneNumber = vendorItem["PhoneNumber"].ToString();
                            }

                            SendOrderNotificationToMembersAndVendors(DiscountedItemValue.LookupValue, vendorName, vendorEmail, vendorPhoneNumber, memberName, memberEmailAddress);
                        }

                        int itemCount = colOrdersCartItems.Count;
                        for (int i = 0; i < itemCount; i++)
                        {
                            colOrdersCartItems[0].Delete();
                        }

                        impersonatedWeb.AllowUnsafeUpdates = false;
                    }
                }
            });

            Response.Redirect("Info.aspx?msg=" + (int)Utility.Messages.Order_Submitted_Successfully);
        }

        protected void cvSubmit_ServerValidate(object source, System.Web.UI.WebControls.ServerValidateEventArgs args)
        {
            args.IsValid = (repCartItems.Items.Count > 0);
        }

        protected void ValidateConfirmation_ServerSide(object sender, ServerValidateEventArgs e)
        {
            e.IsValid = cbxConfirmation.Checked;
        }

        private void SendOrderNotificationToMembersAndVendors(string productName, string vendorName, string vendorEmail, string vendorPhoneNumber, string memberName, string memberEmailAddress)
        {
            string senderEmailSignature = SPUtility.GetLocalizedString("$Resources:SenderEmailSignature", "Club0803_Config", 1033);
            string senderEmailAddress = SPUtility.GetLocalizedString("$Resources:SenderEmailAddress", "Club0803_Config", 1033);

            ListDictionary lstReplacements = new ListDictionary();
            lstReplacements.Add("<%" + EmailReplacementKeys.VendorName.ToString() + "%>", vendorName);
            lstReplacements.Add("<%" + EmailReplacementKeys.VendorEmail.ToString() + "%>", vendorEmail);
            lstReplacements.Add("<%" + EmailReplacementKeys.VendorPhoneNumber.ToString() + "%>", vendorPhoneNumber);
            lstReplacements.Add("<%" + EmailReplacementKeys.ProductName.ToString() + "%>", productName);
            lstReplacements.Add("<%" + EmailReplacementKeys.MemberName.ToString() + "%>", memberName);
            lstReplacements.Add("<%" + EmailReplacementKeys.EmailAddress.ToString() + "%>", memberEmailAddress);
            lstReplacements.Add("<%" + EmailReplacementKeys.SenderEmailSignature.ToString() + "%>", senderEmailSignature);

            string subjectToVendor = SPUtility.GetLocalizedString("$Resources:OrderSubmissionNotificationToVendorEmailSubject", "Club0803_Config", 1033);
            string bodyToVendor = SPUtility.GetLocalizedString("$Resources:OrderSubmissionNotificationToVendorEmailBody", "Club0803_Config", 1033);

            string subjectToMember = SPUtility.GetLocalizedString("$Resources:OrderSubmissionNotificationToMembersEmailSubject", "Club0803_Config", 1033);
            string bodyToMember = SPUtility.GetLocalizedString("$Resources:OrderSubmissionNotificationToMembersEmailBody", "Club0803_Config", 1033);

            EMailHandler.SendEmail(subjectToVendor, bodyToVendor, vendorEmail, fromAddress: senderEmailAddress, replacements: lstReplacements);
            EMailHandler.SendEmail(subjectToMember, bodyToMember, memberEmailAddress, fromAddress: senderEmailAddress, replacements: lstReplacements);
        }

    }
}
