﻿using System;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using Microsoft.SharePoint.WebControls;
using Microsoft.SharePoint.WebPartPages;

namespace TAVIA.SharePoint.Club0803.SitePagesModule
{
    public partial class Default : WebPartPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                SPRibbon ribbon = SPRibbon.GetCurrent(this.Page);
                if (ribbon != null)
                {
                    ribbon.Visible = false;
                }

                string AdminsGroupName = SPUtility.GetLocalizedString("$Resources:AdminsGroupName", "Club0803_Config", 1033);
                string SuperAdminsGroupName = SPUtility.GetLocalizedString("$Resources:SuperAdminsGroupName", "Club0803_Config", 1033);

                if (SPContext.Current.Web.SiteGroups[AdminsGroupName].ContainsCurrentUser ||
                    SPContext.Current.Web.SiteGroups[SuperAdminsGroupName].ContainsCurrentUser)
                {
                    divAdmin1.Visible = true;
                    divAdmin2.Visible = true;
                    divAdmin3.Visible = true;
                    divAdmin4.Visible = true;
                    divAdmin5.Visible = true;
                    divAdmin6.Visible = true;
                    divAdmin7.Visible = true;
                    divAdmin8.Visible = true;
                    divAdmin9.Visible = true;
                }

                SetMyCartCount();

            }
        }

        private void SetMyCartCount()
        {
            //lblMyCartCount
            SPUser currentUser = SPContext.Current.Web.CurrentUser;

            SPSecurity.RunWithElevatedPrivileges(delegate ()
            {
                using (SPSite impersonatedSite = new SPSite(SPContext.Current.Web.Url))
                {
                    using (SPWeb impersonatedWeb = impersonatedSite.OpenWeb())
                    {
                        impersonatedWeb.AllowUnsafeUpdates = true;

                        string OrdersCartListInternalName = SPUtility.GetLocalizedString("$Resources:OrdersCartListUrl", "Club0803_Config", 1033);
                        string OrdersCartListtUrl = String.Format("{0}/lists/{1}", impersonatedWeb.Url, OrdersCartListInternalName);
                        SPList OrdersCartList = impersonatedWeb.GetList(OrdersCartListtUrl);

                        SPQuery query = new SPQuery();
                        query.Query = String.Format("<OrderBy><FieldRef Name=\"ID\" Ascending=\"FALSE\" /></OrderBy><Where><Eq><FieldRef Name=\"EmployeeName\" LookupId=\"TRUE\" /><Value Type=\"Lookup\">{0}</Value></Eq></Where>", currentUser.ID);
                        query.ViewFields = "<FieldRef Name=\"ID\" /><FieldRef Name=\"EmployeeName\" />";
                        query.ViewFieldsOnly = true;
                        query.RowLimit = 1000;

                        SPListItemCollection colMyCartItems = OrdersCartList.GetItems(query);

                        lblMyCartCount.Text = String.Format(" ({0})", colMyCartItems.Count);

                        impersonatedWeb.AllowUnsafeUpdates = false;
                    }
                }
            });
        }
    }
}
