﻿<%@ Assembly Name="TAVIA.SharePoint.Club0803, Version=1.0.0.0, Culture=neutral, PublicKeyToken=e94667b04745ceb1" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MyOrders.aspx.cs" Inherits="TAVIA.SharePoint.Club0803.SitePagesModule.MyOrders" MasterPageFile="~masterurl/default.master" %>

<asp:Content ID="PageHead" ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">
    <style>
        #sideNavBox {
            display: none;
        }

        #contentBox {
            margin-right: 20px;
            margin-left: 20px;
        }

        .h4, h4 {
            font-size: 1.5rem;
        }
    </style>

    <SharePoint:UIVersionedContent ID="UIVersionedContent1" UIVersion="4" runat="server">
        <contenttemplate>
		<SharePoint:CssRegistration Name="forms.css" runat="server"/>
	</contenttemplate>
    </SharePoint:UIVersionedContent>

    <link href="/_layouts/15/club0803/styles/bootstrap.min.css" rel="stylesheet" />
    <link href="/_layouts/15/club0803/styles/bootstrap-grid.min.css" rel="stylesheet" />
    <link href="/_layouts/15/club0803/styles/bootstrap-reboot.min.css" rel="stylesheet" />
    <link href="/_layouts/15/club0803/styles/style.css" rel="stylesheet" />
    <link href="/_layouts/15/club0803/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <link href="/_layouts/15/club0803/styles/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="/_layouts/15/club0803/styles/responsive.dataTables.min.css" rel="stylesheet" />

    <script src="/_layouts/15/club0803/js/jquery.min.js"></script>
    <script src="/_layouts/15/club0803/js/bootstrap.min.js"></script>
    <script src="/_layouts/15/club0803/js/script.js"></script>
    <script src="/_layouts/15/club0803/js/jquery.dataTables.min.js"></script>
    <script src="/_layouts/15/club0803/js/dataTables.responsive.min.js"></script>

</asp:Content>

<asp:Content ID="Main" ContentPlaceHolderID="PlaceHolderMain" runat="server">
    <section class="col-md-12 form-container pb-3">
        <h2 class="title-container text-center py-3">My Orders</h2>
        <div class="form-bg">
            <div class="row p-3">
                <div class="col-lg-12 col-md-12 col-sm-12 col-12 pt-3">
                    <table id="example" class="display table-bordered responsive nowrap" style="width: 100%">
                        <thead style="background-color: black; color: #fff;">
                            <tr>
                                <th>Order ID</th>
                                <th>Discounted Item</th>
                                <th>Vendor</th>
                                <th>Order Date</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                              <asp:Repeater ID="repOrders" runat="server">
                                   <ItemTemplate>
                                        <tr>
                                            <td><%# DataBinder.Eval(Container.DataItem, "Title") %></td>
                                            <td><%# DataBinder.Eval(Container.DataItem, "DiscountedItem") %></td>
                                            <td><%# DataBinder.Eval(Container.DataItem, "Vendor") %></td>
                                            <td><%# ((DateTime)DataBinder.Eval(Container.DataItem, "Created")).ToString("dd MMM yyyy") %></td>
                                            <td><a href='ViewDiscountedItem.aspx?ID=<%# DataBinder.Eval(Container.DataItem, "DiscountedItemID") %>'>View/Rate</a></td>
                                        </tr>
                                   </ItemTemplate>
                              </asp:Repeater>
                        </tbody>
                    </table>
                </div>

            </div>

            <div class="col-md-12">
                <hr class="mtn-divider m-0 px-3" />
            </div>
            <!-- button section -->
            <div class="col-md-12 py-3">
                <div class="row btn-div text-center">
                    <asp:Button ID="btnCancel" class="btn mtn-btn mr-3" CausesValidation="false" runat="server" Text="Go to Home" OnClick="btnCancel_Click" />
                </div>
            </div>
        </div>
    </section>

    <script>
        $(document).ready(function () {
            $('#example').DataTable();
        });
    </script>

</asp:Content>

<asp:Content ID="PageTitle" ContentPlaceHolderID="PlaceHolderPageTitle" runat="server">
    Club0803 - My Orders
</asp:Content>

<asp:Content ID="PageTitleInTitleArea" ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server" />
