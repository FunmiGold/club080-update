﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TAVIA.SharePoint.Club0803
{
    [Serializable]
    public class TempDiscountedItem
    {

        public int Id { get; set; }
        public string ProductName { get; set; }
        public string ProductDescription { get; set; }
        public string VendorProductWebsiteURL { get; set; }
        public string ClosingDate { get; set; }
        public string PaymentOptions { get; set; }
        public int InterestRate { get; set; }
        public int DiscountRate { get; set; }
        public string OtherImportantInformation { get; set; }
        public string ProductPrice { get; set; }
        public string Status { get; set; }

        //public bool IsNew { get; set; }
    }
}
