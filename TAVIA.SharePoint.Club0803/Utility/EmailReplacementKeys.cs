﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TAVIA.SharePoint.Club0803.Utility
{
    public enum EmailReplacementKeys
    {
        DiscountedItemNumber, DiscountedItemUrl, Action, DiscountedItemCategory,
        ExpirationDate, ProductName, ProductDescription, CardNumber, CardOwner, CardOwnerEmailAddress,
        VendorName, VendorEmail, VendorPhoneNumber, MemberName, EmailAddress,
        SenderEmailSignature
    }
}
