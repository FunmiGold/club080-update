﻿<%@ Assembly Name="TAVIA.SharePoint.Club0803, Version=1.0.0.0, Culture=neutral, PublicKeyToken=e94667b04745ceb1" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Info.aspx.cs" Inherits="TAVIA.SharePoint.Club0803.SitePagesModule.Info" MasterPageFile="~masterurl/default.master" %>

<asp:Content ID="PageHead" ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">
    <style>
        #sideNavBox {
            display: none;
        }

        #contentBox {
            margin-right: 20px;
            margin-left: 20px;
        }

        .h4, h4 {
            font-size: 1.5rem;
        }
    </style>

    <SharePoint:UIVersionedContent ID="UIVersionedContent1" UIVersion="4" runat="server">
        <contenttemplate>
		<SharePoint:CssRegistration Name="forms.css" runat="server"/>
	</contenttemplate>
    </SharePoint:UIVersionedContent>

    <link href="/_layouts/15/club0803/styles/bootstrap.min.css" rel="stylesheet" />
    <link href="/_layouts/15/club0803/styles/bootstrap-grid.min.css" rel="stylesheet" />
    <link href="/_layouts/15/club0803/styles/bootstrap-reboot.min.css" rel="stylesheet" />
    <link href="/_layouts/15/club0803/styles/style.css" rel="stylesheet" />
    <link href="/_layouts/15/club0803/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

</asp:Content>

<asp:Content ID="Main" ContentPlaceHolderID="PlaceHolderMain" runat="server">
    <section class="col-md-12 form-container pb-3">
        <h2 class="title-container text-center py-3">Welcome to Club0803</h2>
        <div class="form-bg">
            <div class="container">
                <div class="row p-3">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-12 py-3 hvr-bounce-out">
                        <div class="alert alert-success" role="alert" >
                                <asp:Label ID="lblMessage" runat="server"></asp:Label>
                         </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <hr class="mtn-divider m-0 px-3" />
            </div>

            <!-- button section -->
            <div class="col-md-12 py-3">
                <div class="row btn-div text-center">
                    <asp:Button ID="btnCancel" class="btn mtn-btn mr-3" CausesValidation="false" runat="server" Text="Go To Home" OnClick="btnCancel_Click" />
                </div>
            </div>

        </div>
    </section>
</asp:Content>

<asp:Content ID="PageTitle" ContentPlaceHolderID="PlaceHolderPageTitle" runat="server">
    Club0803
</asp:Content>

<asp:Content ID="PageTitleInTitleArea" ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server" />
