﻿<%@ Assembly Name="TAVIA.SharePoint.Club0803, Version=1.0.0.0, Culture=neutral, PublicKeyToken=e94667b04745ceb1" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewDiscountedItem.aspx.cs" Inherits="TAVIA.SharePoint.Club0803.SitePagesModule.ViewDiscountedItem" MasterPageFile="~masterurl/default.master" %>

<asp:Content ID="PageHead" ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">
    <style>
        #sideNavBox {
            display: none;
        }

        #contentBox {
            margin-right: 20px;
            margin-left: 20px;
        }

        .h4, h4 {
            font-size: 1.5rem;
        }
    </style>

    <SharePoint:UIVersionedContent ID="UIVersionedContent1" UIVersion="4" runat="server">
        <contenttemplate>
		<SharePoint:CssRegistration Name="forms.css" runat="server"/>
	</contenttemplate>
    </SharePoint:UIVersionedContent>

    <link href="/_layouts/15/club0803/styles/bootstrap.min.css" rel="stylesheet" />
    <link href="/_layouts/15/club0803/styles/bootstrap-grid.min.css" rel="stylesheet" />
    <link href="/_layouts/15/club0803/styles/bootstrap-reboot.min.css" rel="stylesheet" />
    <link href="/_layouts/15/club0803/styles/style.css" rel="stylesheet" />
    <link href="/_layouts/15/club0803/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <link href="/_layouts/15/club0803/styles/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="/_layouts/15/club0803/styles/responsive.dataTables.min.css" rel="stylesheet" />
    <link href="/_layouts/15/club0803/styles/owl.carousel.min.css" rel="stylesheet" />

    <script src="/_layouts/15/club0803/js/jquery.min.js"></script>
    <script src="/_layouts/15/club0803/js/bootstrap.min.js"></script>
    <script src="/_layouts/15/club0803/js/script.js"></script>
    <script src="/_layouts/15/club0803/js/jquery.dataTables.min.js"></script>
    <script src="/_layouts/15/club0803/js/dataTables.responsive.min.js"></script>
    <script src="/_layouts/15/club0803/js/owl.carousel.min.js"></script>

</asp:Content>

<asp:Content ID="Main" ContentPlaceHolderID="PlaceHolderMain" runat="server">
    <section class="col-md-12 form-container pb-3">
        <h2 class="title-container text-center py-3">Discounted Item Details</h2>
        <div class="form-bg">
            <div class="col-md-12">
                    <div class="row justify-content-center">
                        <div class="col-md-12 text-center mb-3 mt-3">
                            <img class="img-fluid" alt="card-img-top" runat="server" id="imgDiscountedItem"></div>
                        <h4 class="text-center"><asp:Label ID="lblProductName" Text="&nbsp;" runat="server"></asp:Label></h4>
                    </div>
                    <div class="col-lg-9 card border-0">
                        <div class="card-body px-2">
                            <p class="text-left d-inline"><h5 class="font-weight-bold">Product Description: </h5>
                                <asp:Label ID="lblProductDescription" Text="&nbsp;" runat="server"></asp:Label>
                            </p>
                            <h5 class="font-weight-bold">Discounted Items Details:</h5>
                            <div class="input-group">
                                <label class="font-weight-bold col-lg-5 col-sm-6 col-12">Category:</label>
                                <p class="col-lg-7 col-sm-6 col-12">
                                    <asp:Label ID="lblCategory" Text="&nbsp;" runat="server"></asp:Label>
                                </p>
                            </div>
                            <div class="input-group">
                                <label class="font-weight-bold col-lg-5 col-sm-6 col-12">Type:</label>
                                <p class="col-lg-7 col-sm-6 col-12"><asp:Label ID="lblCategoryType" Text="&nbsp;" runat="server"></asp:Label></p>
                            </div>
                            <div class="input-group">
                                <label class="font-weight-bold col-lg-5 col-sm-6 col-12">Vendor Name:</label>
                                <p class="col-lg-7 col-sm-6 col-12"><asp:Label ID="lblVendorName" Text="&nbsp;" runat="server"></asp:Label></p>
                            </div>
                            <div class="input-group">
                                <label class="font-weight-bold col-lg-5 col-sm-6 col-12">Contact Person:</label>
                                <p class="col-lg-7 col-sm-6 col-12"><asp:Label ID="lblVendorContactPerson" Text="&nbsp;" runat="server"></asp:Label></p>
                            </div>
                                                     <div class="input-group">
                                <label class="font-weight-bold col-lg-5 col-sm-6 col-12">Phone Number:</label>
                                <p class="col-lg-7 col-sm-6 col-12"><asp:Label ID="lblVendorPhoneNumber" Text="&nbsp;" runat="server"></asp:Label></p>
                            </div>
                            <div class="input-group">
                                <label class="font-weight-bold col-lg-5 col-sm-6 col-12">Contact Email:</label>
                                <p class="col-lg-7 col-sm-6 col-12"><asp:Label ID="lblVendorContactEmail" Text="&nbsp;" runat="server"></asp:Label></p>
                            </div>
                            <div class="input-group">
                                <label class="font-weight-bold col-lg-5 col-sm-6 col-12">Contact Address:</label>
                                <p class="col-lg-7 col-sm-6 col-12"><asp:Label ID="lblVendorContactAddress" Text="&nbsp;" runat="server"></asp:Label></p>
                            </div>
                            <div class="input-group">
                                <label class="font-weight-bold col-lg-5 col-sm-6 col-12">Vendor/Product Website URL:</label>
                                <p class="col-lg-7 col-sm-6 col-12"><asp:HyperLink ID="hlVendorProductWebsiteURL" runat="server" Target="_blank"></asp:HyperLink></p>
                            </div>
                            <div class="input-group">
                                <label class="font-weight-bold col-lg-5 col-sm-6 col-12">Closing Date:</label>
                                <p class="col-lg-7 col-sm-6 col-12"><asp:Label ID="lblClosingDate" Text="&nbsp;" runat="server"></asp:Label></p>
                            </div>
                            <div class="input-group">
                                <label class="font-weight-bold col-lg-5 col-sm-6 col-12">Payment Options:</label>
                                <p class="col-lg-7 col-sm-6 col-12"><asp:Label ID="lblPaymentOptions" Text="&nbsp;" runat="server"></asp:Label></p>
                            </div>
                            <div class="input-group">
                                <label class="font-weight-bold col-lg-5 col-sm-6 col-12">Status:</label>
                                <p class="col-lg-7 col-sm-6 col-12"><asp:Label ID="lblStatus" Text="&nbsp;" runat="server"></asp:Label></p>
                            </div>
                            <div class="input-group">
                                <label class="font-weight-bold col-lg-5 col-sm-6 col-12">Interest Rate:</label>
                                <p class="col-lg-7 col-sm-6 col-12"><asp:Label ID="lblInterestRate" Text="&nbsp;" runat="server"></asp:Label></p>
                            </div>
                            <div class="input-group">
                                <label class="font-weight-bold col-lg-5 col-sm-6 col-12">Discount Rate:</label>
                                <p class="col-lg-7 col-sm-6 col-12"><asp:Label ID="lblDiscountRate" Text="&nbsp;" runat="server"></asp:Label></p>
                            </div>
                            <div class="input-group">
                                <label class="font-weight-bold col-lg-5 col-sm-6 col-12">Any other important information:</label>
                                <p class="col-lg-7 col-sm-6 col-12"><asp:Label ID="lblOtherImportantInformation" Text="&nbsp;" runat="server"></asp:Label></p>
                            </div>
                            <div class="input-group">
                                <label class="font-weight-bold col-lg-5 col-sm-6 col-12">Product Price:</label>
                                <p class="col-lg-7 col-sm-6 col-12"><asp:Label ID="lblProductPrice" Text="&nbsp;" runat="server"></asp:Label></p>
                            </div>
                        <div class="input-group" id="divRating1" runat="server" visible="false">
                            <div class="col-md-4">
                                <label class="font-weight-bold">Discounted Item Rating:</label>
                            </div>
                            <div class="col-md-8">
                                <asp:RadioButtonList ID="rblDiscountedItemRating" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Value="1" Text="1"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="2"></asp:ListItem>
                                    <asp:ListItem Value="3" Text="3"></asp:ListItem>
                                    <asp:ListItem Value="4" Text="4"></asp:ListItem>
                                    <asp:ListItem Value="5" Text="5"></asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </div>
                        <div class="input-group" id="divRating2" runat="server" visible="false">
                            <div class="col-md-4">
                                <label class="font-weight-bold">Vendor Response Rating:</label>
                            </div>
                            <div class="col-md-8">
                                <asp:RadioButtonList ID="rblVendorResponseRating" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Value="1" Text="1"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="2"></asp:ListItem>
                                    <asp:ListItem Value="3" Text="3"></asp:ListItem>
                                    <asp:ListItem Value="4" Text="4"></asp:ListItem>
                                    <asp:ListItem Value="5" Text="5"></asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </div>
                        <div class="input-group" id="divRating3" runat="server" visible="false">
                            <div class="col-md-4">
                                <label class="font-weight-bold">Vendor Complaints Satisfaction Rating:</label>
                            </div>
                            <div class="col-md-8">
                                <asp:RadioButtonList ID="rblVendorComplaintsRating" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Value="1" Text="1"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="2"></asp:ListItem>
                                    <asp:ListItem Value="3" Text="3"></asp:ListItem>
                                    <asp:ListItem Value="4" Text="4"></asp:ListItem>
                                    <asp:ListItem Value="5" Text="5"></asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div class="col-md-12 text-center">
                         <div class="row btn-div text-center">
                            <asp:Button ID="btnSubmitRating" class="btn mtn-btn mr-3" runat="server" Text="Submit/Update Rating" OnClick="btnSubmitRating_Click" Visible="false" />
                            <asp:Button ID="btnAddToCart" class="btn mtn-btn mr-3" runat="server" Text="Add to Cart" OnClick="btnAddToCart_Click" Visible="false" OnClientClick="return confirm('Are you sure you want to add this item?');" />
                            <asp:Button ID="btnRemoveFromCart" class="btn mtn-btn mr-3" runat="server" Text="Remove from Cart" OnClick="btnRemoveFromCart_Click" Visible="false" OnClientClick="return confirm('Are you sure you want to remove this item?');" />
                            <asp:Button ID="btnClose" class="btn mtn-btn mr-3" CausesValidation="false" runat="server" Text="Back" OnClick="btnClose_Click" />
                        </div>
                    </div>
                </div>
        </div>
    </section>


</asp:Content>

<asp:Content ID="PageTitle" ContentPlaceHolderID="PlaceHolderPageTitle" runat="server">
    Club0803 - Discounted Item Details
</asp:Content>

<asp:Content ID="PageTitleInTitleArea" ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server" />
