﻿using System;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;

namespace TAVIA.SharePoint.Club0803.DiscountedItems
{
    public partial class EditItem : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Redirect("DisplayItem.aspx?ID=" + SPContext.Current.ItemId);
        }
    }
}
